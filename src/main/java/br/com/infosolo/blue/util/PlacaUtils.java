package br.com.infosolo.blue.util;

import org.apache.commons.lang3.StringUtils;

public class PlacaUtils {

	public static String mask(String placa) {

		if (StringUtils.isBlank(placa)) {
			return placa;
		}

		placa = placa.trim().toUpperCase();

		if (placa.length() == 7) {
			String letras = placa.substring(0, 3);
			String numeros = placa.substring(3, placa.length());

			placa = letras + "-" + numeros;
		}

		return placa;
	}

	public static String unmask(String placa) {
		return StringUtils.isBlank(placa) ? placa : placa.replace("-", "").toUpperCase().trim();
	}

	public static boolean isValid(String placa) {
		return StringUtils.isBlank(placa) ? false : placa.trim().matches("^[a-zA-Z]{3}\\-\\d{4}$");
	}

}
