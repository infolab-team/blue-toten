package br.com.infosolo.blue.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public class SenhaUtil {
	
	public static String encrypt(String senha) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		return new String(Hex.encodeHex(DigestUtils.md5(senha)));
	}
	
}
