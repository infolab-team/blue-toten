package br.com.infosolo.blue.util;

public class Mensagens {

	public static final String ERRO = "Erro: ";
	public static final String USUARIO_NAO_ENCONTRADO = "Usu\u00E1rio n\u00E3o encontrado.";
	public static final String ERRO_INTERNO_SERVIDOR = "Um erro ocorreu. Tente novamente.";
	public static final String NAO_FOI_POSSIVEL_OBTER_ACCESS_TOKEN = "N\u00E3o foi poss\u00EDvel obter o Access Token para o Usu\u00E1rio informado.";
	public static final String DIGITE_CPF_CNPJ_OU_EMAIL_VALIDOS = "Digite um CPF, CNPJ ou email v\u00E1lidos.";
	public static final String USUARIO_NAO_POSSUI_SALDO_SUFICIENTE = "Usu\u00E1rio n\u00E3o possui saldo suficiente para realizar essa transa\u00e7\u00E3o.";
	
}
