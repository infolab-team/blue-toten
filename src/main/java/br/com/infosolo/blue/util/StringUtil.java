package br.com.infosolo.blue.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class StringUtil {

	public static String removerAcentos(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[^\\p{ASCII}]", "");
		return str;
	}

	public static String converterUtf8(String str) {
		if (str != null) {
			int n = Charset.forName("UTF-8").encode(str).limit();
			byte[] b = Charset.forName("UTF-8").encode(str).array();
			
			try {
				str = new String(b, 0, n, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				System.out.println("Excessão -> " + e);
			}
		}
		
		return str;
	}

	public static List<String> converterUtf8(List<String> msgs) {
		List<String> retorno = new ArrayList<String>();

		for (String str : msgs) {
			if (str != null) {
				int n = Charset.forName("UTF-8").encode(str).limit();
				byte[] b = Charset.forName("UTF-8").encode(str).array();
				
				try {
					retorno.add(new String(b, 0, n, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					System.out.println("Excessão -> " + e);
				}
			}
		}
		
		return retorno;
	}
	
}
