package br.com.infosolo.blue.util;

import java.util.Map;
import java.util.Date;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

public class QuartzUtil {

	public QuartzUtil() {
	}

	private static Scheduler scheduler;

	public static Scheduler getScheduler() throws SchedulerException {
		if (scheduler == null) {
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
		}
		
		return scheduler;
	}

	public static void agendar(String nome, String grupo, Date dataHora, Class<? extends Job> classe, Map<String, Object> parametros) {
		try {

			JobDetail job = JobBuilder.newJob(classe).withIdentity(nome, grupo).build();

			if (parametros != null) {
				for (Map.Entry<String, Object> entry : parametros.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();

					job.getJobDataMap().put(key, value);
				}
			}

			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(nome, grupo).startAt(dataHora).build();
			getScheduler().scheduleJob(job, trigger);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void renovarJobCheckinCheckout(String nome, Date data) throws SchedulerException {
		TriggerKey tk = new TriggerKey(nome, "checkout");
		getScheduler().rescheduleJob(getScheduler().getTrigger(tk).getKey(),TriggerBuilder.newTrigger().withIdentity(nome, "checkout").startAt(data).build());
	}

}
