package br.com.infosolo.blue.rest;

import java.io.IOException;

import javax.security.sasl.AuthenticationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

public class RequestFilter implements ContainerRequestFilter {

	@Context
    private ResourceInfo resourceInfo;
	
	@Override
	public void filter(ContainerRequestContext containerRequest) throws IOException {
		String method = containerRequest.getMethod();
		
		UriInfo uriInfo = containerRequest.getUriInfo();
		String path = uriInfo.getPath();
		String pathParams = uriInfo.getPathParameters().toString();
		String queryParams = uriInfo.getQueryParameters().toString();
		
		String version = containerRequest.getHeaderString("version");
		
		MediaType mt = containerRequest.getMediaType();
		String mtStr = "";

		if (mt != null && mt.getSubtype() != "") {
			mtStr = "(" + mt.getSubtype() + ")";
		}

		String strLog = mtStr + "[" + method + "] on BLUE-TOTEM: " + path;

		System.out.println("\n==================================================================================================================");
		System.out.println(strLog);
		
		System.out.println("PathParams: " + pathParams);
		System.out.println("URLParams: " + queryParams);
		System.out.println("Version: " + version);
		System.out.println("\n\n\n");

		if(true){
			throw new AuthenticationException("Em cumprimento a decisão do TCE/TO o Estacionamento Rotativo em Palmas está suspenso temporariamente.");
		}
		
	}
	
}