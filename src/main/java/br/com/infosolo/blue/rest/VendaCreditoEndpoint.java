package br.com.infosolo.blue.rest;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;

import br.com.infosolo.blue.model.Tablet;
import br.com.infosolo.blue.model.Transacao;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.VendaStone;
import br.com.infosolo.blue.model.enums.TipoPagamento;
import br.com.infosolo.blue.model.enums.TipoTransacao;
import br.com.infosolo.blue.service.TabletService;
import br.com.infosolo.blue.service.TransacaoService;
import br.com.infosolo.blue.service.UsuarioService;
import br.com.infosolo.blue.service.VendaCreditoService;
import br.com.infosolo.blue.util.Mensagens;

@Path("/credito")
@Produces(MediaType.APPLICATION_JSON)
public class VendaCreditoEndpoint {

	@Inject
	private Logger logger;
	
	@Inject
	private VendaCreditoService service;
	
	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private TabletService tabletService;
	
	@Inject
	private TransacaoService transacaoService;
	
	@POST
	@Path("/comprar")
	public Response vender(@FormParam("identificador") String identificador, 
			@FormParam("valor") BigDecimal valor,
			@FormParam("tipoPagamento") TipoPagamento tipoPagamento, 
			@FormParam("arn") String arn,
			@FormParam("ca") String ca, 
			@FormParam("bandeira") String bandeira,
			@FormParam("imei") String imei) {
		
		System.out.println("\n\n----------------------------------------------------------------");
		logger.log(Level.INFO, "Entrou no metodo comprar()");
		logger.log(Level.INFO, "Identificador: " + identificador);
		logger.log(Level.INFO, "Valor: " + valor.toString());
		logger.log(Level.INFO, "Tipo Pagamento: " + tipoPagamento.getDescricao());
		logger.log(Level.INFO, "ARN: " + arn);
		logger.log(Level.INFO, "CA: " + ca);
		logger.log(Level.INFO, "Bandeira: " + bandeira);
		logger.log(Level.INFO, "IMEI Totem: " + imei);
		System.out.println("----------------------------------------------------------------\n\n");
		
		try {
			Usuario usuario = null;

			if (identificador.contains("@")) {
				usuario = usuarioService.findByEmail(identificador);
			} else {
				usuario = usuarioService.findByCpfouCnpj(identificador);
			}

			logger.log(Level.INFO, "Comprador: " + usuario.getIdentificador());
			
			VendaStone stone = null;

			if (tipoPagamento.equals(TipoTransacao.C) || tipoPagamento.equals(TipoTransacao.D)) {

				boolean validacaoStone = StringUtils.isNotBlank(arn) ? StringUtils.isNotBlank(ca) : false;
				logger.log(Level.INFO, "Validacao Stone: " + validacaoStone);
				
				if (!validacaoStone) {
					return Response.status(Response.Status.BAD_REQUEST).entity("Informe o ARN e CA").header("Accept-Charset", "UTF-8").build();
				}
				
				stone = new VendaStone(arn, ca, bandeira);
				logger.log(Level.INFO, "Stone: " + stone.toString());
			}

			Tablet tablet = tabletService.porImei(imei);
			Transacao transacao = service.efetivar(usuario, tablet, valor, tipoPagamento, stone);

			BigDecimal saldo = transacaoService.saldo(usuario);
			
			System.out.println("Saldo do usuário: " + saldo);
			
			usuario.setSaldo(saldo);
			transacao.setUsuario(usuario);
			
			return Response.ok(transacao).build();

		} catch (NoResultException ne) {
			return Response.status(Response.Status.BAD_REQUEST).entity(Mensagens.USUARIO_NAO_ENCONTRADO).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

}
