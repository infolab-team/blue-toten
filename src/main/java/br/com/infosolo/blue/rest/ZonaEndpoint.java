package br.com.infosolo.blue.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.model.Zona;
import br.com.infosolo.blue.service.ZonaService;

@Path("/zona")
@Produces(MediaType.APPLICATION_JSON)
public class ZonaEndpoint {

	@Inject
	private ZonaService zonaService;
	
	@GET
	@Path("/findAll")
	public Response todos() {
		try {
			List<Zona> zonas = zonaService.getAll();
			return Response.ok(zonas).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("/proximas")
	public Response getZonasProximas(@QueryParam("lat") String lat, @QueryParam("lng") String lng) {
		try {
			List<Zona> zonas = new ArrayList<>();
			
			if(lat != null || lng != null){
				zonas = zonaService.getProximasPorCoordenada(lat, lng);
			} else{
				zonas = zonaService.getAll();
			}
			
			return Response.ok(zonas).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}
	
}
