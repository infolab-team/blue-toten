package br.com.infosolo.blue.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.infosolo.blue.model.Checkin;
import br.com.infosolo.blue.model.Tpu;
import br.com.infosolo.blue.model.Transacao;
import br.com.infosolo.blue.service.CheckinService;
import br.com.infosolo.blue.service.TpuService;
import br.com.infosolo.blue.service.TransacaoService;

@Path("/comprovante")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class ComprovanteEndpoint {
	
	
	@Inject
	private TpuService tpuService;
	@Inject
	private CheckinService checkinService;
	@Inject
	private TransacaoService transacaoService;
	

	@POST
	@Path("/tpu")
	public Response enviaComprovantePagamentoTPU(@FormParam("email") String email, @FormParam("idTpu") Integer idTpu){
		System.out.println("email: " + email + ", idTpu: " + idTpu);
		
		if(email == null || email.isEmpty()){
			return Response.status(Status.BAD_REQUEST).entity("Informe o email para receber o comprovante.").build();
		}
		
		if(idTpu == null || idTpu == 0){
			return Response.status(Status.BAD_REQUEST).entity("Informe o número da TPU que foi paga.").build();
		}
		
		Tpu tpu = tpuService.busca(idTpu);
		
		if(tpu  == null){
			return Response.status(Status.BAD_REQUEST).entity("TPU não encontrada.").build();
		}
		
		tpuService.enviaRecibo(tpu, email);
		
		System.out.println("Sucesso ao enviar comprovante de pagamento de tpu");
		
		return Response.ok().build();
	}
	
	
	
	@POST
	@Path("/checkin")
	public Response enviaComprovanteCheckin(@FormParam("email") String email, @FormParam("idCheckin") Integer idCheckin){
		System.out.println("idCheckin: " + idCheckin + ", email: " + email + "\n");
		
		if(email == null || email.isEmpty()){
			return Response.status(Status.BAD_REQUEST).entity("Informe o email para receber o comprovante.").build();
		}
		
		if(idCheckin == null || idCheckin == 0){
			return Response.status(Status.BAD_REQUEST).entity("Informe o número do checkin.").build();
		}
		
		Checkin checkin = checkinService.load(idCheckin);
		
		if(checkin  == null){
			return Response.status(Status.BAD_REQUEST).entity("Checkin não encontrado.").build();
		}
		
		checkinService.enviaRecibo(checkin, email);
		
		return Response.ok().build();
	}
	
	
	@POST
	@Path("/compraCredito")
	public Response enviaComprovanteCompraCredito(@FormParam("email") String email, @FormParam("idTransacao") Integer idTransacao){
		if(email == null || email.isEmpty()){
			return Response.status(Status.BAD_REQUEST).entity("Informe o email para receber o comprovante.").build();
		}
		
		if(idTransacao == null || idTransacao == 0){
			return Response.status(Status.BAD_REQUEST).entity("Informe o número da transação.").build();
		}
		
		Transacao transacao = transacaoService.load(idTransacao);		
		
		
		if(transacao  == null){
			return Response.status(Status.BAD_REQUEST).entity("Transação não encontrada.").build();
		}
		
		transacaoService.enviaRecibo(transacao, email);
				
		return Response.ok().build();
	}
}
