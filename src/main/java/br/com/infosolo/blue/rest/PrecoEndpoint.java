package br.com.infosolo.blue.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.model.Preco;
import br.com.infosolo.blue.service.PrecoService;

@Path("/preco")
@Produces(MediaType.APPLICATION_JSON)
public class PrecoEndpoint {

	@Inject
	private PrecoService precoService;
	
    @GET
    @Path("/findAll")
    public Response todos() {
        try {
        	List<Preco> precos = precoService.findAll();
			return Response.ok(precos).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
    }

}