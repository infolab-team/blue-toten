package br.com.infosolo.blue.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.model.TipoVeiculo;
import br.com.infosolo.blue.service.TipoVeiculoService;

@Path("/tipo-veiculo")
@Produces(MediaType.APPLICATION_JSON)
public class TipoVeiculoEndpoint {

	@Inject
	private TipoVeiculoService tipoVeiculoService;
	
	@GET
	@Path("/")
	public Response getTiposVeiculos(){
		List<TipoVeiculo> tipos = tipoVeiculoService.findAllByPracaId(1);
		return Response.ok().entity(tipos).build();
	}
}
