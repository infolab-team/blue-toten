package br.com.infosolo.blue.rest;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.model.Tablet;
import br.com.infosolo.blue.service.TabletService;
import br.com.infosolo.blue.util.Mensagens;

@Path("/tablet")
@Produces(MediaType.APPLICATION_JSON)
public class TabletEndpoint {

	@Inject
	private TabletService tabletService;

	@GET
	@Path("/todos")
	public Response todos() {
		try {
			List<Tablet> tablets = tabletService.todos();
			return Response.ok(tablets).build();
			
		} catch (WebApplicationException e) {
			e.printStackTrace();
			throw new WebApplicationException(Mensagens.ERRO_INTERNO_SERVIDOR);
		}
	}

	@GET
	@Path("/por-imei/{imei}")
	public Response porImei(@PathParam("imei") String numero) {
		try {
			Tablet tablet = tabletService.porImei(numero);
			return Response.ok(tablet).build();
			
		} catch (WebApplicationException e) {
			e.printStackTrace();
			throw new WebApplicationException(Mensagens.ERRO_INTERNO_SERVIDOR);
		}
	}

	@POST
	@Path("/cadastro")
	public Response cadastro(@FormParam("imei") String numero, 
							@FormParam("nome") String nome, 
							@FormParam("versao") String versao,
							@FormParam("lat") String lat,
							@FormParam("lng") String lng) {
		try {
			Tablet tablet = tabletService.cria(new Tablet(numero, nome, versao, lat, lng));
			return Response.ok(tablet).build();
			
		} catch (WebApplicationException e) {
			throw new WebApplicationException(Mensagens.ERRO_INTERNO_SERVIDOR);
		}
	}

	@POST
	@Path("/edicao")
	public Response edicao(@FormParam("imei") String imei, 
						@FormParam("nome") String nome, 
						@FormParam("versao") String versao,
						@FormParam("lat") String lat,
						@FormParam("lng") String lng,
						@FormParam("idAgente") Integer idAgente) {
		try {
			Tablet tablet = tabletService.editar(imei, nome, versao, lat, lng, idAgente);
			return Response.ok(tablet).build();
			
		} catch (WebApplicationException e) {
			throw new WebApplicationException(Mensagens.ERRO_INTERNO_SERVIDOR);
		}
	}

}