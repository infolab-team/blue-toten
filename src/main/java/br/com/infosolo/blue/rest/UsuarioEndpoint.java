package br.com.infosolo.blue.rest;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.service.UsuarioService;
import br.com.infosolo.blue.util.Mensagens;

@Path("/usuario")
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioEndpoint {

	@Inject
	private UsuarioService usuarioService;

	@GET
	@Path("/find/{param}")
	public Response findByIdentificador(@PathParam("param") String identificador) {
		try {
			Usuario usuario = null;

			if (identificador.contains("@")) {
				usuario = usuarioService.findByEmail(identificador);
			} else {
				usuario = usuarioService.findByCpfouCnpj(identificador);
			}

			return Response.ok(usuario).build();
		} catch (NoResultException ne) {
			return Response.status(Response.Status.NO_CONTENT).entity(ne.getMessage()).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

	@POST
	@Path("/auth")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response auth(@FormParam("param") String param, @FormParam("senha") String senha) {
		Usuario usuario = new Usuario();

		try {
			usuario.setSenha(senha);

			if (param.contains("@")) {
				usuario.setEmail(param);
				usuario = usuarioService.findByEmailAndPassword(usuario);
			} else {
				usuario.setIdentificador(param);
				usuario = usuarioService.findByIdentificadorAndPassword(usuario);
			}

			return Response.ok(usuario).build();
			
		} catch (NoResultException ne) {
			return Response.status(Response.Status.BAD_REQUEST).entity(Mensagens.USUARIO_NAO_ENCONTRADO).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}

	}

}
