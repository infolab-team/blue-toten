package br.com.infosolo.blue.rest;

import java.util.List;

import javax.inject.Inject;
import javax.swing.plaf.synth.SynthSeparatorUI;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;

import br.com.infosolo.blue.model.Tpu;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.VendaStone;
import br.com.infosolo.blue.model.dto.TpuDTO;
import br.com.infosolo.blue.model.enums.TipoPagamento;
import br.com.infosolo.blue.service.TabletService;
import br.com.infosolo.blue.service.TpuService;
import br.com.infosolo.blue.service.UsuarioService;
import br.com.infosolo.blue.service.VendaCreditoService;
import br.com.infosolo.blue.util.Mensagens;

/**
 * Provedor de serviços Rest para TPUs.
 *
 * @author Leonardo da Matta
 * @author Kelcio
 */
@Path("/tpu")
@Produces(MediaType.APPLICATION_JSON)
public class TpuEndpoint {

	@Inject
	private TpuService tpuService;
	
	@Inject
	private VendaCreditoService vendaService;
	
	@Inject
	private UsuarioService usuarioService;
	
	@GET
    @Path("/regularizar/{placa}")
    public Response regularizar(@PathParam("placa") String placa) {
		List<TpuDTO> tpus = tpuService.porPlaca(placa);       
		Tpu act = tpuService.busca(tpus.get(0).getId());
		//Xml xml = AreaTecnologia.efetivarRegularizacao(act.getPlaca(), act.getNumero(), new Date());
		
		return Response.ok().entity("").build();
    }
	
    @GET
    @Path("/por-placa/{placa}")
    public Response porPlaca(@PathParam("placa") String placa) {
    	try {
			List<TpuDTO> tpus = tpuService.porPlaca(placa);
			return tpus.isEmpty() ? Response.status(Status.BAD_REQUEST).build() : Response.ok(tpus).build();
			
		} catch (WebApplicationException e) {
			throw new WebApplicationException(Mensagens.ERRO_INTERNO_SERVIDOR);
		}
    }

    @POST
    @Path("/pagar")
    public Response pagamento(
    		@FormParam("idTpu") Integer idTpu, 
    		@FormParam("idUsuario") Integer idUsuario, 
    		@FormParam("tipoPagamento") String tipoPagamentoStr, 
			@FormParam("arn") String arn, 
			@FormParam("ca") String ca,
			@FormParam("bandeira") String bandeira,
			@FormParam("imei") String numero) {

    	System.out.println("idTpu: " + idTpu);
    	System.out.println("idUsuario: " + idUsuario);
    	System.out.println("tipoPagamento: " + tipoPagamentoStr);
    	System.out.println("arn: " + idTpu);
    	System.out.println("ca: " + idTpu);
    	System.out.println("bandeira: " + idTpu);
    	System.out.println("imei: " + numero);
    	
    	try {
    		Tpu tpu = tpuService.busca(idTpu);
    		TipoPagamento tipoPagamento = TipoPagamento.parse(tipoPagamentoStr);
    		
    		if(tipoPagamento == null){
    			tipoPagamento = TipoPagamento.getByDescricao(tipoPagamentoStr);
    		}
    		
    		if (TipoPagamento.B.equals(tipoPagamento) || (idUsuario != null && (tipoPagamentoStr == null || tipoPagamentoStr.contains("Blue")))) {
    			System.out.println("PAGAMENTO DE TPU POR CREDITO BLUE");
    			Usuario usuario = usuarioService.load(idUsuario);
    			
    			if (!tpuService.usuarioTemSaldoParaPagarTpu(usuario)) {
    				return Response.status(Status.BAD_REQUEST).entity(Mensagens.USUARIO_NAO_POSSUI_SALDO_SUFICIENTE).build();
    			}
    			
    			tpu = vendaService.pagamentoComCreditoBlue(tpu, usuario);
    			
			} else {
				System.out.println("PAGAMENTO DE TPU POR CARTAO");
                VendaStone stone = null;

                if (StringUtils.isNotBlank(arn) && StringUtils.isNotBlank(ca)) {
                    stone = new VendaStone(arn, ca, bandeira);
                }
                
                vendaService.pagamentoComCartaoDeCredito(tpu, tipoPagamento, stone);
			}
    		
            return Response.ok(tpu).build();
            
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new WebApplicationException(Mensagens.ERRO_INTERNO_SERVIDOR);
        }
	}

}