package br.com.infosolo.blue.rest;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.model.Zona;
import br.com.infosolo.blue.service.VagaService;

@Path("/vaga")
@Produces(MediaType.APPLICATION_JSON)
public class VagaEndpoint {

	@Inject
	private VagaService service;
	
	@GET
	@Path("/findbyzona/{idZona}")
	public Response porZona(@PathParam("idZona") Integer idZona) {
		try {
			System.out.println("idZona: " + idZona);
			
			Zona zona = new Zona(idZona);
			return Response.ok(service.findVagasLivresByZona(zona)).build();
			
		} catch (EJBException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}
	
}
