package br.com.infosolo.blue.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.infosolo.blue.core.exception.ValidateException;
import br.com.infosolo.blue.model.Checkin;
import br.com.infosolo.blue.service.facade.CheckinFacade;

@Path("/checkin")
@Produces(MediaType.APPLICATION_JSON)
public class CheckinEndpoint {

	@Inject
	private CheckinFacade checkinFacade;
	
	@POST
	@Path("/efetuar")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response efetuar(@FormParam("idZona") Integer idZona, 
			@FormParam("idTipoVeiculo") Integer idTipoVeiculo, 
			@FormParam("idUsuario") Integer idUsuario, 
			@FormParam("placa") String placa,
			@FormParam("permanencia") Integer permanencia, 
			@FormParam("tipoCheckin") String tipoCheckin,
			@FormParam("tipoPagamento") String tipoPagamento, 
			@FormParam("arn") String arn, 
			@FormParam("ca") String ca,
			@FormParam("bandeira") String bandeira,
			@FormParam("imei") String imei) {

		System.out.println("idZona: " + idZona);
		System.out.println("idTipoVeiculo: " + idTipoVeiculo);
		System.out.println("idUsuario: " + idUsuario);
		System.out.println("placa: " + placa);
		System.out.println("permanencia: " + permanencia);
		System.out.println("tipoCheckin: " + tipoCheckin);
		System.out.println("tipoPagamento: " + tipoPagamento);
		System.out.println("ca: " + ca);
		System.out.println("bandeira: " + bandeira);
		System.out.println("imei: " + imei);
		System.out.println("arn: " + arn + "\n");
		
		try {
			Checkin checkin = checkinFacade.efetuarCheckin(idZona, idTipoVeiculo, idUsuario, placa, permanencia, tipoCheckin, tipoPagamento, arn, ca, bandeira, imei);
			return Response.ok(checkin).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			String mensagemDeErro = (e instanceof ValidateException) ? e.getMessage() : "Um erro ocorreu. Tente novamente.";
			return Response.status(Response.Status.BAD_REQUEST).entity(mensagemDeErro).build();
		}
	}

}