package br.com.infosolo.blue.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.infosolo.blue.interfaces.ModelEntity;

@Entity
@Table(name = "tablet")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Tablet.findAll", query = "SELECT t FROM Tablet t"),
		@NamedQuery(name = "Tablet.findByImei", query = "SELECT t FROM Tablet t WHERE t.imei = :imei") })
public class Tablet implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@NotEmpty(message = "Informe o IMEI")
	@Column(name = "imei")
	private String imei;

	@NotEmpty(message = "Informe o nome")
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "dt_atualizacao")
	private Date dataAtualizacao;
	
	@Column(name = "latitude")
	private BigDecimal latitude;
		
	@Column(name = "longitude")
	private BigDecimal longitude;
	
	@Column(name = "versao")
	private String versao;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario_atualizacao")
	private Usuario agenteAtualizacao;
	
	public Tablet() {

	}

	public Tablet(String imei, String nome, String versao, String lat, String lng) {
		super();
		this.imei = imei;
		this.nome = nome;
		this.versao = versao;
		this.dataAtualizacao = new Date();

		if(lat != null && !lat.isEmpty()){
			this.latitude = new BigDecimal(lat);
		}
		
		if(lng != null && !lng.isEmpty()){
			this.longitude = new BigDecimal(lng);
		}
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		setId(id);
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public Usuario getAgenteAtualizacao() {
		return agenteAtualizacao;
	}

	public void setAgenteAtualizacao(Usuario agenteAtualizacao) {
		this.agenteAtualizacao = agenteAtualizacao;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.imei).toHashCode();
	}

	@Override
	public boolean equals(Object object) {

		if (object == null) {
			return false;
		}

		if (!(object instanceof Tablet)) {
			return false;
		}

		Tablet other = (Tablet) object;
		return new EqualsBuilder().append(imei, other.getId()).isEquals();
	}

}