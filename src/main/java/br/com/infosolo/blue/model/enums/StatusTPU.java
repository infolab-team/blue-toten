package br.com.infosolo.blue.model.enums;

public enum StatusTPU {

	R("Registrada"), 
	Q("Quitada"), 
	D("Denuncia"), 
	C("Cancelada"), 
	T("Todos");

	private final String descricao;
	
	private StatusTPU(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
