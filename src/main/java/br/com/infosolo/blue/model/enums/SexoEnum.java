package br.com.infosolo.blue.model.enums;


public enum SexoEnum {
	M("Masculino"), F("Feminino");

	private SexoEnum(String value) {
		this.value = value;
	}

	private final String value;

	public String getValue() {
		return value;
	}
	

}
