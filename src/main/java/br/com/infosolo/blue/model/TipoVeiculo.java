package br.com.infosolo.blue.model;

import br.com.infosolo.blue.interfaces.ModelEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "tipo_veiculo")
@XmlRootElement
@NamedQuery(name = "TipoVeiculo.findAll", query = "SELECT t FROM TipoVeiculo t where t.praca.id = :pracaId")
public class TipoVeiculo implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Column(name = "nome")
	private String nome;

	@ManyToOne
    @JoinColumn(name = "id_praca", referencedColumnName = "id")
	private Praca praca;
	
	public TipoVeiculo() {
		
	}
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Praca getPraca() {
		return praca;
	}

	public void setPraca(Praca praca) {
		this.praca = praca;
	}

	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof TipoVeiculo)) {
            return false;
        }

        TipoVeiculo other = (TipoVeiculo) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
	
}
