package br.com.infosolo.blue.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.SubTipoVaga;
import br.com.infosolo.blue.model.enums.TipoVaga;

@Entity
@Table(name = "vaga")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Vaga.findByZona", query = "SELECT v FROM Vaga v where v.zona.numero = :numeroZona order by v.numero"),
        @NamedQuery(name = "Vaga.findByTipo", query = "SELECT v FROM Vaga v WHERE v.numero = :numero and v.zona.id = :zona and v.tipo = :tipoVaga"),
        @NamedQuery(name = "Vaga.findByTipoSubTipo", query = "SELECT v FROM Vaga v WHERE v.numero = :numero and v.zona.id = :zona and v.tipo = :tipoVaga and v.subTipo = :subTipoVaga")})
public class Vaga implements ModelEntity<Integer>, Comparable<Vaga>{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "numero")
    private Integer numero;

    @JsonIgnore
    @ManyToOne(optional = false , fetch = FetchType.LAZY)
    @JoinColumn(name = "id_zona_blue")
    private Zona zona;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vaga")
    private List<Checkin> checkinList;

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo")
    private TipoVaga tipo;

    @Basic(optional = true)
    @Enumerated(EnumType.STRING)
    @Column(name = "subtipo")
    private SubTipoVaga subTipo;

    public Vaga() {
    	
    }
    
    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public List<Checkin> getCheckinList() {
		return checkinList;
	}

	public void setCheckinList(List<Checkin> checkinList) {
		this.checkinList = checkinList;
	}

	public TipoVaga getTipo() {
		return tipo;
	}

	public void setTipo(TipoVaga tipo) {
		this.tipo = tipo;
	}

	public SubTipoVaga getSubTipo() {
		return subTipo;
	}

	public void setSubTipo(SubTipoVaga subTipo) {
		this.subTipo = subTipo;
	}

	@Override
    public int compareTo(Vaga that) {
        final int ANTES = -1;
        final int IGUAL = 0;
        final int DEPOIS = 1;

        if (this.equals(that)) {
            return IGUAL;
        }

        if ((this.id != null && that.getId() != null) && (this.id == that.getId())) {
            return IGUAL;
        }

        final int tipoThis = vagaParaComparacao(this);
        final int tipoThat = vagaParaComparacao(that);

        if (tipoThis == tipoThat) {
            return this.numero.compareTo(that.numero);

        } else if (tipoThis < tipoThat) {
            return ANTES;
        } else {
            return DEPOIS;
        }
    }

    private int vagaParaComparacao(Vaga v) {
        return (v.getTipo().equals(TipoVaga.N) || v.getTipo().equals(TipoVaga.D)
                || v.getTipo().equals(TipoVaga.I) || v.getTipo().equals(TipoVaga.C)
                || v.getTipo().equals(TipoVaga.T)) ? 1 // se tipo for veiculo, recebe 1
                : v.subTipo.equals(SubTipoVaga.A) ? 2 // se for moto A, recebe 2
                : 3; // se for moto B, recebe 3
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Vaga)) {
            return false;
        }

        Vaga other = (Vaga) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
}
