package br.com.infosolo.blue.model.enums;

public enum OrigemTransacao {
	
	A("Android"),
	I("iOS"),
	IOS("iOS"),
	W("WEB"),
	PV("Ponto de Venda"),
	TT("Totem");
	
	OrigemTransacao(String value) {
		this.value = value;
	}
	
	private final String value;

	public String getValue() {
		return value;
	}

}
