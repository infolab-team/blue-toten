package br.com.infosolo.blue.model.enums;

/**
 * Representação dos diferentes tipos de pagamento.
 *
 * @author Leonardo da Matta.
 */
public enum TipoPagamento {
	
	M("Dinheiro"),
	C("Cr\u00E9dito"),
	D("D\u00E9bito"),
	P("Mercado Pago"),
	B("Cr\u00E9dito Blue");

	private final String descricao;

	private TipoPagamento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

    /**
     * Informa se o tipo em questão está contido em uma lista de tipos.
     *
     * @param tipos Tipos para comparação.
     * @return <b>true</b> caso o tipo esteja contido na lista de tipos ou <b>false</b> caso contrário.
     */
    public boolean estaContido(TipoPagamento... tipos) {
        for (TipoPagamento tipo : tipos) {
            if (this.equals(tipo)) {
                return true;
            }
        }
        
        return false;
    }
    
    public static TipoPagamento parse(String descricao) {
        for (TipoPagamento categoria : TipoPagamento.values()) {
            if (categoria.toString().equals(descricao)) {
                return categoria;
            }
        }
        
        return null;
    }
    
    public static TipoPagamento getByDescricao(String descricao){
    	for (TipoPagamento tipo : TipoPagamento.values()) {
			if(tipo.getDescricao().equals(descricao)){
				return tipo;
			}
		}
    	
    	return null;
    }
    
}