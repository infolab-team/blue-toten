package br.com.infosolo.blue.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonManagedReference;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.CategoriaTransacaoEnum;
import br.com.infosolo.blue.model.enums.OrigemVenda;
import br.com.infosolo.blue.model.enums.TipoPagamento;

@Entity
@Table(name = "venda")
@XmlRootElement
public class Venda implements ModelEntity<Integer> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Column(name = "valor")
	private BigDecimal valor;

	@Column(name = "nota_legal")
	private Boolean notaLegal;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pagamento")
	private TipoPagamento tipoPagamento = TipoPagamento.D;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora")
	private Date dataHora = new Date();

	@Enumerated(EnumType.STRING)
	@Column(name = "origem")
	private OrigemVenda origem;
	
	@ManyToOne
	@JoinColumn(name = "id_agente_venda")
	private Usuario agenteVenda;

	@ManyToOne
	@JoinColumn(name = "id_comprador")
	private Usuario comprador;

	@ManyToOne
	@JoinColumn(name = "id_checkin_checkout")
	private Checkin checkin;

	@OneToOne
	@JoinColumn(name = "id_tablet")
	private Tablet tablet;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "categoria")
	private CategoriaTransacaoEnum categoria;
	
	@ManyToOne
	@JoinColumn(name = "id_tpu")
	@JsonManagedReference
	private Tpu tpu;
	
	public Venda() {

	}

	public Venda(Double valor, TipoPagamento tipoPagamento){
		this.valor = new BigDecimal(valor);
		this.tipoPagamento = tipoPagamento;
		this.dataHora = new Date();
	}
	
	public Venda(Integer id) {
		this.id = id;
	}

	public Venda(Integer id, BigDecimal valor) {
		this.id = id;
		this.valor = valor;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Boolean getNotaLegal() {
		return notaLegal;
	}

	public void setNotaLegal(Boolean notaLegal) {
		this.notaLegal = notaLegal;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public OrigemVenda getOrigem() {
		return origem;
	}

	public void setOrigem(OrigemVenda origem) {
		this.origem = origem;
	}

	public Usuario getAgenteVenda() {
		return agenteVenda;
	}

	public void setAgenteVenda(Usuario agenteVenda) {
		this.agenteVenda = agenteVenda;
	}

	public Usuario getComprador() {
		return comprador;
	}

	public void setComprador(Usuario comprador) {
		this.comprador = comprador;
	}

	public Checkin getCheckin() {
		return checkin;
	}

	public void setCheckin(Checkin checkin) {
		this.checkin = checkin;
	}

	public Tablet getTablet() {
		return tablet;
	}

	public void setTablet(Tablet tablet) {
		this.tablet = tablet;
	}

	public boolean temComprador() {
		return this.getComprador() != null;
	}
	
	public CategoriaTransacaoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaTransacaoEnum categoria) {
		this.categoria = categoria;
	}

	public Tpu getTpu() {
		return tpu;
	}

	public void setTpu(Tpu tpu) {
		this.tpu = tpu;
	}

	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Venda)) {
            return false;
        }

        Venda other = (Venda) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

}
