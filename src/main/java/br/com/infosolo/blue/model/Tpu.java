/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package br.com.infosolo.blue.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonBackReference;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.StatusTPU;
import br.com.infosolo.blue.util.PlacaUtils;

/**
 * 
 * @author diego
 */
@Entity
@Table(name = "tpu")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "TPU.findByDataHoraRange", query = "select t from Tpu t where t.dataHora between :dataInicio AND :dataFim order by t.dataHora"),
		@NamedQuery(name = "TPU.findByPlaca", query = "select t from Tpu t where t.placa = :placa"),
		@NamedQuery(name = "TPU.findByDataHora", query = "select t from Tpu t where t.dataHora = :dataHora"),
		@NamedQuery(name = "TPU.findByStatus", query = "select t from Tpu t where t.status = :status") })
public class Tpu implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "act_id_seq")
	@SequenceGenerator(name = "act_id_seq", sequenceName = "act_id_seq", allocationSize = 1)
	private Integer id;

	@NotNull(message = "O número do auto é obrigatório")
	@Column(name = "auto", length = 10, unique = true)
	private String auto;
	
	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora")
	private Date dataHora = new Date();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_pagamento")
	private Date dataHoraPagamento;
	
	@Column(name = "cod_infracao")
	private String codigoInfracao;
	
	@Enumerated(EnumType.STRING)
	@Basic(optional = false)
	@Column(name = "status")
	private StatusTPU status = StatusTPU.R;
	
	@Basic(optional = false)
	@Column(name = "placa")
	private String placa;
	
	@Column(name = "marca", length = 50)
	private String marca;
	
	@Column(name = "especie", length = 50)
	private String especie;
	
	@Column(name = "cor", length = 50)
	private String cor;
	
	@Column(name = "local", length=100)
	private String local;
	
	@Column(name = "tipo_local")
	private String tipoLocal;
	
	@Column(name = "ponto_referencia", length=100)
	private String pontoReferencia;
	
	@Column(name = "bairro", length=50)
	private String bairro;
	
	@Column(name = "cidade")
	private String cidade;
	
//	@NotNull(message = "Informe a UF")
//	@Enumerated(EnumType.STRING)
//	@Column(name = "uf")
//	private UF uf;

	@Column(name = "desdobramento")
	private Integer desdobramento;

	@ManyToOne
	@JoinColumn(name = "id_agente_emissor")
	private Usuario agenteEmissor;

	@OneToOne
	@JoinColumn(name = "id_venda")
	@JsonBackReference
	private Venda venda;

	@Basic(optional = false)
	@Column(name = "lat")
	private Double lat;
		
	@Basic(optional = false)
	@Column(name = "lng")
	private Double lng;
	
	@Basic(optional = false)
	@Column(name = "valor")
	private Double valor;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario_pagamento")
	private Usuario usuario;
	
	//@OneToOne
	//@NotNull(message = "A TPU precisa ter um aviso de irregularidade associado")
	//@JoinColumn(name = "id_aviso", referencedColumnName = "id")
	//private AvisoIrregularidade aviso;
	
	@Transient
	private Usuario usuarioProprietario;
	
	public Tpu() {
		this.status = StatusTPU.R;
		this.valor = new Double(10);
	}

	public Tpu(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigoInfracao() {
		return codigoInfracao;
	}

	public void setCodigoInfracao(String codigoInfracao) {
		this.codigoInfracao = codigoInfracao;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public Date getDataHoraPagamento() {
		return dataHoraPagamento;
	}

	public void setDataHoraPagamento(Date dataHoraPagamento) {
		this.dataHoraPagamento = dataHoraPagamento;
	}

	public StatusTPU getStatus() {
		return status;
	}

	public void setStatus(StatusTPU status) {
		this.status = status;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getTipoLocal() {
		return tipoLocal;
	}

	public void setTipoLocal(String tipoLocal) {
		this.tipoLocal = tipoLocal;
	}

	public String getPontoReferencia() {
		return pontoReferencia;
	}

	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = pontoReferencia;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Integer getDesdobramento() {
		return desdobramento;
	}

	public void setDesdobramento(Integer desdobramento) {
		this.desdobramento = desdobramento;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Usuario getAgenteEmissor() {
		return agenteEmissor;
	}

	public void setAgenteEmissor(Usuario agenteEmissor) {
		this.agenteEmissor = agenteEmissor;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public String getAuto() {
		return auto;
	}

	public void setAuto(String auto) {
		this.auto = auto;
	}

	@PrePersist
	@PreUpdate
	public void formatar() {
		if (!StringUtils.isBlank(this.placa)) {
			this.placa = PlacaUtils.mask(this.placa.toUpperCase().trim());
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).toHashCode();
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}

		if (!(object instanceof Tpu)) {
			return false;
		}

		Tpu other = (Tpu) object;
		return new EqualsBuilder().append(id, other.getId()).isEquals();
	}

	@Override
	public String toString() {
		return "Tpu [id=" + id + ", codigoInfracao=" + codigoInfracao + "]";
	}

	public Usuario getUsuarioProprietario() {
		return usuarioProprietario;
	}

	public void setUsuarioProprietario(Usuario usuarioProprietario) {
		this.usuarioProprietario = usuarioProprietario;
	}

}
