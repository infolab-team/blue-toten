package br.com.infosolo.blue.model;

/**
 * Created by fabioestrela on 17/07/15.
 */
public class EntityModel {

    private String saldo;
    private String tempo;
    private String exception;

    private boolean erro;


    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
        setErro(true);
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
        setErro(true);
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
        setErro(true);
    }

    public boolean isErro() {
        return erro;
    }

    public void setErro(boolean erro) {
        this.erro = erro;
    }
    
}
