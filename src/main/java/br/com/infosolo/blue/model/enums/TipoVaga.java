package br.com.infosolo.blue.model.enums;

import java.util.Arrays;
import java.util.List;

import br.com.infosolo.blue.model.Vaga;

public enum TipoVaga {

	N("Normal"), D("PNE"), I("Idoso"), M("Moto"), C("Caminh\u00E3o"), T("T\u00E1xi"), X("Moto-T\u00E1xi"), G("Geral");

	private TipoVaga(String descricao) {
		this.descricao = descricao;
	}

	private final String descricao;

	public String getDescricao() {
		return descricao;
	}

    /**
     * Busca os tipos isentos de cobrança.
     *
     * @return Lista de tipos isentos.
     */
    public static List<TipoVaga> tiposIsentos() {
        return Arrays.asList(T, X, C);
    }

    public static boolean vagaIsenta(Vaga vaga) {
        return TipoVaga.tiposIsentos().contains(vaga.getTipo());
    }

    public static boolean vagaNaoIsenta(Vaga vaga) {
        return !vagaIsenta(vaga);
    }
    
}
