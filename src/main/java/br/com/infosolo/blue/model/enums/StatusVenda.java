package br.com.infosolo.blue.model.enums;

public enum StatusVenda {
	
	C("Concluida"), 
	A("Aguardando Confirmacao");

	private final String descricao;

	private StatusVenda(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
