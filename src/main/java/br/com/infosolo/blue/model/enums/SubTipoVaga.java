package br.com.infosolo.blue.model.enums;

public enum SubTipoVaga {

    A("MotoA"), 
    B("MotoB"), 
    N("TaxiCarro");

	private final String descricao;

	private SubTipoVaga(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
