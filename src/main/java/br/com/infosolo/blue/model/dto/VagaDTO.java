package br.com.infosolo.blue.model.dto;

import br.com.infosolo.blue.model.enums.SubTipoVaga;
import br.com.infosolo.blue.model.enums.TipoVaga;

/**
 * Created by fabioestrela on 24/06/15.
 */
public class VagaDTO {

	private Integer numero;
	private TipoVaga tipo;
	private SubTipoVaga subTipo;

	public VagaDTO(Integer numero, TipoVaga tipo, SubTipoVaga subTipo) {
		this.numero = numero;
		this.tipo = tipo;
		this.subTipo = subTipo;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public TipoVaga getTipo() {
		return tipo;
	}

	public void setTipo(TipoVaga tipo) {
		this.tipo = tipo;
	}

	public SubTipoVaga getSubTipo() {
		return subTipo;
	}

	public void setSubTipo(SubTipoVaga subTipo) {
		this.subTipo = subTipo;
	}
	
}
