package br.com.infosolo.blue.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.OrigemVenda;

@Entity
@Table(name = "checkin_checkout")
@XmlRootElement
@NamedQuery(name = "Checkin.findAll", query = "SELECT c FROM Checkin c")
public class Checkin implements ModelEntity<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_checkin", nullable = false)
    private Date dataHoraCheckin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_checkout")
    private Date dataHoraCheckout;

    @Column(name = "tempo_reservado_minutos", nullable = false)
    private Integer tempoReservadoMinutos;

    @Enumerated(EnumType.STRING)
    @Column(name = "origem")
    private OrigemVenda origem = OrigemVenda.TT;

    @ManyToOne
    @JoinColumn(name = "id_zona_blue", referencedColumnName = "id", nullable = false)
    private Zona zona;

    @ManyToOne
    @JoinColumn(name = "id_vaga", referencedColumnName = "id", nullable = true)
    private Vaga vaga;

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "id_usuario_logado", referencedColumnName = "id")
    private Usuario usuarioLogado;

    @ManyToOne
    @JoinColumn(name = "id_tipo_veiculo", referencedColumnName = "id")
    private TipoVeiculo tipoVeiculo;

    @Column(name = "cpf")
    private String identificador;

    @Column(name = "placa")
    private String placa;

    @OneToOne
	@JoinColumn(name = "id_tablet")
	private Tablet tablet;
    
    @ManyToOne
    @JoinColumn(name = "id_praca", referencedColumnName = "id")
	private Praca praca;
    
    @JsonIgnore
    @OneToOne
    private Transacao transacao;
    
    public Checkin() {
    	
    }
    
    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataHoraCheckin() {
        return dataHoraCheckin;
    }

    public void setDataHoraCheckin(Date dataHoraCheckin) {
        this.dataHoraCheckin = dataHoraCheckin;
    }

    public Date getDataHoraCheckout() {
        return dataHoraCheckout;
    }

    public void setDataHoraCheckout(Date dataHoraCheckout) {
        this.dataHoraCheckout = dataHoraCheckout;
    }

    public Integer getTempoReservadoMinutos() {
        return tempoReservadoMinutos;
    }

    public void setTempoReservadoMinutos(Integer tempoReservadoMinutos) {
        this.tempoReservadoMinutos = tempoReservadoMinutos;
    }

    public OrigemVenda getOrigem() {
        return origem;
    }

    public void setOrigem(OrigemVenda origem) {
        this.origem = origem;
    }

    public Vaga getVaga() {
        return vaga;
    }

    public void setVaga(Vaga vaga) {
        this.vaga = vaga;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
       this.zona = zona;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoVeiculo getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
    public Tablet getTablet() {
		return tablet;
	}

	public void setTablet(Tablet tablet) {
		this.tablet = tablet;
	}

	public Praca getPraca() {
		return praca;
	}

	public void setPraca(Praca praca) {
		this.praca = praca;
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}

	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Checkin)) {
            return false;
        }

        Checkin other = (Checkin) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
}
