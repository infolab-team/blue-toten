package br.com.infosolo.blue.model.dto;

import java.math.BigDecimal;

public class UsuarioDTO {

	private String identificador;
	private String email;
	private BigDecimal saldo;

	public UsuarioDTO() {
		
	}
	
	public UsuarioDTO(String identificador, String email, BigDecimal saldo) {
		this.identificador = identificador;
		this.email = email;
		this.saldo = saldo;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

}
