package br.com.infosolo.blue.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.CategoriaTransacaoEnum;
import br.com.infosolo.blue.model.enums.OrigemTransacao;
import br.com.infosolo.blue.model.enums.TipoTransacao;

@Entity
@Table(name = "transacao")
@XmlRootElement
@NamedQuery(name = "Transacao.findValorByUsuario", query = "SELECT sum(t.valor) FROM Transacao t WHERE t.usuario.id = :idUsuario and t.praca.id = 1 and t.pago = true")
public class Transacao implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Column(name = "valor")
	private BigDecimal valor;

	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_transacao")
	private TipoTransacao tipoTransacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora")
	private Date dataHora;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_venda", referencedColumnName = "id")
	private Venda venda;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_checkin_checkout", referencedColumnName = "id")
	private Checkin checkin;

	@ManyToOne
    @JoinColumn(name = "id_praca", referencedColumnName = "id")
	private Praca praca;
	
	@Column(name = "pago")
	private Boolean pago;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "origem")
	private OrigemTransacao origem;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "categoria")
	private CategoriaTransacaoEnum categoria;
	
	public Transacao() {
		
	}
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoTransacao getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(TipoTransacao tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public Checkin getCheckin() {
		return checkin;
	}

	public void setCheckin(Checkin checkin) {
		this.checkin = checkin;
	}
	
	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Transacao)) {
            return false;
        }

        Transacao other = (Transacao) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

	public Boolean getPago() {
		return pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public Praca getPraca() {
		return praca;
	}

	public void setPraca(Praca praca) {
		this.praca = praca;
	}

	public OrigemTransacao getOrigem() {
		return origem;
	}

	public void setOrigem(OrigemTransacao origem) {
		this.origem = origem;
	}

	public CategoriaTransacaoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaTransacaoEnum categoria) {
		this.categoria = categoria;
	}

}
