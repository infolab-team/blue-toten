package br.com.infosolo.blue.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.infosolo.blue.interfaces.ModelEntity;

@Entity
@Table(name = "praca")
public class Praca implements ModelEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nome")
	private String nome;
	
	@Column(name = "latitude")
	private BigDecimal latitude;
	
	@Column(name = "longitude")
	private BigDecimal longitude;
	
	@Column(name = "sigla")
	private String sigla;

	@Column(name = "nome_un_monetaria_praca")
	private String nomeUnidadeMonetaria;
	
	@Column(name = "valor_un_monetaria_praca")
	private Double valorUnidadeMonetaria;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNomeUnidadeMonetaria() {
		return nomeUnidadeMonetaria;
	}

	public void setNomeUnidadeMonetaria(String nomeUnidadeMonetaria) {
		this.nomeUnidadeMonetaria = nomeUnidadeMonetaria;
	}

	public Double getValorUnidadeMonetaria() {
		return valorUnidadeMonetaria;
	}

	public void setValorUnidadeMonetaria(Double valorUnidadeMonetaria) {
		this.valorUnidadeMonetaria = valorUnidadeMonetaria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Praca other = (Praca) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Praca [id=" + id + ", nome=" + nome + "]";
	}
	
	
}
