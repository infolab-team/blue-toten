package br.com.infosolo.blue.model;

import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.AtivoInativo;

@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Usuario.findById", query = "SELECT u FROM Usuario u WHERE u.id = :id"),
		@NamedQuery(name = "Usuario.findByCpfouCnpj", query = "SELECT u FROM Usuario u WHERE u.identificador = :identificador and u.status = br.com.infosolo.blue.model.enums.AtivoInativo.A"),
		@NamedQuery(name = "Usuario.findByEmail", query = "SELECT u FROM Usuario u WHERE u.email = :email and u.status = br.com.infosolo.blue.model.enums.AtivoInativo.A"),
		@NamedQuery(name = "Usuario.findByIdentificadorAndPassword", query = "SELECT u FROM Usuario u WHERE u.identificador = :identificador and u.senha = :senha and u.status = br.com.infosolo.blue.model.enums.AtivoInativo.A"),
		@NamedQuery(name = "Usuario.findByEmailAndPassword", query = "SELECT u FROM Usuario u WHERE u.email = :email and u.senha = :senha and u.status = br.com.infosolo.blue.model.enums.AtivoInativo.A") })
public class Usuario implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Column(name = "cpf")
	private String identificador;

	@Basic(optional = true)
	@Column(name = "email")
	private String email;

	@Basic(optional = false)
	@Column(name = "senha")
	private String senha;

	@Column(name = "saldo")
	private BigDecimal saldo;

	@Basic(optional = false)
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private AtivoInativo status = AtivoInativo.I;

	public Usuario() {
		
	}
	
	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}
	
	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Usuario)) {
            return false;
        }

        Usuario other = (Usuario) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
	
}
