package br.com.infosolo.blue.model.enums;

public enum TipoZona {

	A("Zona Azul", "#45A2EF"), V("Zona Verde", "#47C70C");

	private final String descricao;
	private final String cor;

	private TipoZona(String descricao, String cor) {
		this.descricao = descricao;
		this.cor = cor;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getCor() {
		return cor;
	}

	public static TipoZona parse(String descricao) {
		for (TipoZona tipo : TipoZona.values()) {
			if (tipo.getDescricao().equals(descricao.trim())) {
				return tipo;
			}
		}

		return null;
	}

}