package br.com.infosolo.blue.model.enums;

public enum CategoriaTransacaoEnum {

	CC(1, "Compra de Créditos"),
	CK(2, "Checkin"),
	VC(3, "Venda de Créditos"),
	PT(4, "Pagamento de TPU"),
	VT(5, "Venda de Talão"),
	BB(6, "Bônus Blue"),
	BT(7, "Bônus Pagamento TPU");
	
	CategoriaTransacaoEnum(int id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	private int id;
	private String nome;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
