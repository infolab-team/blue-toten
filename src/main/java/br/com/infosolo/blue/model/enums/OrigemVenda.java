package br.com.infosolo.blue.model.enums;

public enum OrigemVenda {

    PV("Ponto de Venda"),
    A("Android"),
    IOS("IOS"),
    WP("Windows Phone"),
    WEB("Portal Web"),
    SMS("Mensagem SMS"),
    AG("Agente"),
    TT("Toten");

	private final String descricao;

    private OrigemVenda(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}