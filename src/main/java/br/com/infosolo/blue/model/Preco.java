package br.com.infosolo.blue.model;

import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.infosolo.blue.interfaces.ModelEntity;

@Entity
@Table(name = "preco")
@XmlRootElement
@NamedQuery(name = "Preco.findAll", query = "SELECT p FROM Preco p WHERE p.quantidade NOT IN(15,45) AND p.tipoVeiculo.id IN(1,2)  ORDER BY p.quantidade")
public class Preco implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Column(name = "qtd_minutos")
	private Integer quantidade;

	@Basic(optional = false)
	@Column(name = "valor")
	private BigDecimal valor;

	@ManyToOne(optional = false)
	@JoinColumn(name = "id_tipo_veiculo", referencedColumnName = "id")
	private TipoVeiculo tipoVeiculo;

	public Preco() {
		
	}
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}
	
	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Preco)) {
            return false;
        }

        Preco other = (Preco) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

}
