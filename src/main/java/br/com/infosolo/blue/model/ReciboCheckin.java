package br.com.infosolo.blue.model;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.SubTipoVaga;
import br.com.infosolo.blue.model.enums.TipoVaga;
import br.com.infosolo.blue.util.SenhaUtil;

@Entity
@Table(name = "recibo_checkin_checkout")
@XmlRootElement
public class ReciboCheckin implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "placa")
    private String placa;

    @Basic(optional = false)
    @Column(name = "veiculo")
    private String veiculo;

    @Column(name = "hash_id")
    private String hashId;

    @Basic(optional = false)
    @Temporal(TemporalType.DATE)
    @Column(name = "data")
    private Date data;

    @Basic(optional = false)
    @Column(name = "valor")
    private BigDecimal valor;

    @Basic(optional = false)
    @Column(name = "vaga")
    private Integer numeroVaga;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_vaga")
    private TipoVaga tipoVaga;

    @Basic(optional = true)
    @Enumerated(EnumType.STRING)
    @Column(name = "subtipo_vaga")
    private SubTipoVaga subTipoVaga;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_checkin_checkout", referencedColumnName = "id")
    private Checkin checkin;

    @OneToOne
    @JoinColumn(name = "numero_zona_blue", referencedColumnName = "numero")
    private Zona zona;

    public ReciboCheckin() {
    	
    }
    
    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getNumeroVaga() {
        return numeroVaga;
    }

    public void setNumeroVaga(Integer numeroVaga) {
        this.numeroVaga = numeroVaga;
    }

    public TipoVaga getTipoVaga() {
        return tipoVaga;
    }

    public void setTipoVaga(TipoVaga tipoVaga) {
        this.tipoVaga = tipoVaga;
    }

    public SubTipoVaga getSubTipoVaga() {
        return subTipoVaga;
    }

    public void setSubTipoVaga(SubTipoVaga subTipoVaga) {
        this.subTipoVaga = subTipoVaga;
    }

    public Checkin getCheckin() {
        return checkin;
    }

    public void setCheckin(Checkin checkin) {
        this.checkin = checkin;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public String criarHashIdDesteRecibo() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return SenhaUtil.encrypt(this.getId() + "");
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof ReciboCheckin)) {
            return false;
        }

        ReciboCheckin other = (ReciboCheckin) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
}
