package br.com.infosolo.blue.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.infosolo.blue.interfaces.ModelEntity;
import br.com.infosolo.blue.model.enums.TipoZona;

@Entity
@Table(name = "zona_blue")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Zona.findAll", query = "select distinct(z) from Zona z order by z.numero"),
        @NamedQuery(name = "Zona.findByNumero", query = "select distinct(z) from Zona z where z.numero = :numero")})
public class Zona implements ModelEntity<Integer>{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "numero")
    private Integer numero;

    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;

    @Basic(optional = false)
    @Column(name = "limite_maximo")
    private Integer limiteMaximo;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo")
    private TipoZona tipo = TipoZona.A;

    @Column(name = "latitude")
    private BigDecimal lat;
    
    @Column(name = "longitude")
    private BigDecimal lng;
    
    @Transient
    private Double distancia;
    
    @Transient
    private List<Vaga> vagas;

    @ManyToOne
    @JoinColumn(name = "id_praca", referencedColumnName = "id")
    private Praca praca;
    
    public Zona() {
    	
    }
    
    public Zona(Integer id, Integer numero, String nome, TipoZona tipo){
    	this.id = id;
    	this.numero = numero;
    	this.nome = nome;
    	this.tipo = tipo;
    	this.vagas = new ArrayList<Vaga>();
    }
    
    public Zona(Integer id) {
		super();
		this.id = id;
	}

	@Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getLimiteMaximo() {
        return limiteMaximo;
    }

    public void setLimiteMaximo(Integer limiteMaximo) {
        this.limiteMaximo = limiteMaximo;
    }

    public TipoZona getTipo() {
        return tipo;
    }

    public void setTipo(TipoZona tipo) {
        this.tipo = tipo;
    }

    public List<Vaga> getVagas() {
        return vagas;
    }

    public void setVagas(List<Vaga> vagas) {
        this.vagas = vagas;
    }
    
	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof Zona)) {
            return false;
        }

        Zona other = (Zona) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

	public Praca getPraca() {
		return praca;
	}

	public void setPraca(Praca praca) {
		this.praca = praca;
	}
    
}
