package br.com.infosolo.blue.model.enums;

/**
 * Representação da situação de uma TPU.
 *
 * @author Leonardo da Matta
 */
public enum SituacaoTpu {

    REGISTRADA(0, "Registrada"),
    QUITADA(1, "Quitada"),
    CANCELADA(2, "Cancelada"),
    MULTA(3, "Multa");

    private Integer codigo;
    private String descricao;

    /**
     * Construtor que recebe um código e uma representação em valor.
     *
     * @param codigo Código da situação
     * @param descricao Descrição da Situação
     */
    private SituacaoTpu(Integer codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    /**
     * Busca a descrição do enum.
     *
     * @return Descrição do enum.
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Busca o código do enum.
     *
     * @return Representação do código do enum.
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * Busca a situação da TPU pelo código. Os valores permitidos são:
     *
     * 0 - REGISTRADA;<br/>
     * 1 - QUITADA;<br/>
     * 2 - CANCELADA;<br/>
     * 3 - MULTA.
     *
     * @param codigo Código que representa a situação da TPU.
     * @return Enum representando a situação do TPU.
     * @throws IllegalArgumentException Caso o status informado seja inválido.
     */
    public static SituacaoTpu porCodigo(int codigo) throws IllegalArgumentException {
        switch (codigo) {
            case 0:
                return REGISTRADA;
            case 1:
                return QUITADA;
            case 2:
                return CANCELADA;
            case 3:
                return MULTA;
            default:
                throw new IllegalArgumentException("O código informado é inválido.");
        }
    }

    @Override
    public String toString() {
        return descricao;
    }
    
}
