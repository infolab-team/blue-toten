package br.com.infosolo.blue.model.enums;

public enum TipoTransacao {

	C("Cr\u00E9dito"), 
	D("D\u00E9bito");

	private final String descricao;
	
	private TipoTransacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
