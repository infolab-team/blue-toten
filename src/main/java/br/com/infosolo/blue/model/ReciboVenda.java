package br.com.infosolo.blue.model;

import br.com.infosolo.blue.interfaces.ModelEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;

@Entity
@Table(name = "recibo_venda")
@XmlRootElement
@NamedQuery(name = "ReciboVenda.findAll", query = "SELECT r FROM ReciboVenda r")
public class ReciboVenda implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	private static final String CNPJ_INFOSOLO = "10.213.834/0003-09";
	private static final String INSCRICAO_MUNICIPAL_INFOSOLO = "2393697";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_emissao")
	private Date dataHoraEmissao;

	@ManyToOne(optional = false)
	@JoinColumn(name = "id_venda", referencedColumnName = "id")
	private Venda venda;

	public ReciboVenda() {
		
	}
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataHoraEmissao() {
		return dataHoraEmissao;
	}

	public void setDataHoraEmissao(Date dataHoraEmissao) {
		this.dataHoraEmissao = dataHoraEmissao;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public static String getCnpjInfosolo() {
		return CNPJ_INFOSOLO;
	}

	public static String getInscricaoMunicipalInfosolo() {
		return INSCRICAO_MUNICIPAL_INFOSOLO;
	}
	
	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (!(object instanceof ReciboVenda)) {
            return false;
        }

        ReciboVenda other = (ReciboVenda) object;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
	
}
