/**
 * 
 */
package br.com.infosolo.blue.model;

import java.io.InputStream;

/**
 * @author Tiago Meneses (tcmeneses@gmail.com)
 * @since Mar 9, 2017
 *
 */
public class Anexo {

	private String nome;
	private Long tamanho;
	private InputStream arquivo;

	public Anexo() {
		
	}
	
	public Anexo(String nome, Long tamanho, InputStream arquivo) {
		super();
		this.nome = nome;
		this.tamanho = tamanho;
		this.arquivo = arquivo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getTamanho() {
		return tamanho;
	}

	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}

	public InputStream getArquivo() {
		return arquivo;
	}

	public void setArquivo(InputStream arquivo) {
		this.arquivo = arquivo;
	}
	
}