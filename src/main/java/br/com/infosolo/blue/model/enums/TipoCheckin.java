package br.com.infosolo.blue.model.enums;

public enum TipoCheckin {

	U("Usuário"), A("Avulso");

	private final String descricao;

	private TipoCheckin(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
