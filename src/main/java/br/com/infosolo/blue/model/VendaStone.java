package br.com.infosolo.blue.model;

import br.com.infosolo.blue.interfaces.ModelEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "venda_stone")
@XmlRootElement
public class VendaStone implements ModelEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@NotNull
	@OneToOne
	@JoinColumn(name = "id_venda", referencedColumnName = "id")
	private Venda venda;

	@NotEmpty(message = "Informe o ARN")
	@Column(name = "arn")
	private String arn;

	@NotEmpty(message = "Informe o CA")
	@Column(name = "ca")
	private String ca;

	@Column(name = "bandeira")
	private String bandeira;

	public VendaStone() {

	}

	public VendaStone(String arn, String ca, String bandeira) {
		this.arn = arn.trim();
		this.ca = ca.trim();
		this.bandeira = bandeira.trim();
	}

	public VendaStone(Venda vendaCredito, String arn, String ca, String bandeira) {
		this.venda = vendaCredito;
		this.arn = arn.trim();
		this.ca = ca.trim();
		this.bandeira = bandeira.trim();
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public String getArn() {
		return arn;
	}

	public void setArn(String arn) {
		this.arn = arn;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).toHashCode();
	}

	@Override
	public boolean equals(Object object) {

		if (object == null) {
			return false;
		}

		if (!(object instanceof VendaStone)) {
			return false;
		}

		VendaStone other = (VendaStone) object;
		return new EqualsBuilder().append(id, other.getId()).isEquals();
	}

	@Override
	public String toString() {
		return "VendaStone [id=" + id + ", venda=" + venda + ", arn=" + arn + ", ca=" + ca + ", bandeira=" + bandeira + "]";
	}
	
}
