package br.com.infosolo.blue.model.enums;

public enum AtivoInativo {

	A("Ativo"), 
	I("Inativo");

	private final String descricao;
	
	private AtivoInativo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
