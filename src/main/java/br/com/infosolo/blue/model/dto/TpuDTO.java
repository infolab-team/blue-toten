package br.com.infosolo.blue.model.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TpuDTO {

	private Integer id;
	private String placa;
	private Date dataHoraEmissao;
	private String dataHoraEmissaoString;
	private Double valor;

	public TpuDTO() {
		
	}
	
	public TpuDTO(Integer id, String placa, Date dataHoraEmissao, Double valor) {
		super();
		this.id = id;
		this.placa = placa;
		this.dataHoraEmissao = dataHoraEmissao;
		this.dataHoraEmissaoString = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dataHoraEmissao);
		this.valor = valor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataHoraEmissao() {
		return dataHoraEmissao;
	}

	public void setDataHoraEmissao(Date dataHoraEmissao) {
		this.dataHoraEmissao = dataHoraEmissao;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getDataHoraEmissaoString() {
		return dataHoraEmissaoString;
	}

	public void setDataHoraEmissaoString(String dataHoraEmissaoString) {
		this.dataHoraEmissaoString = dataHoraEmissaoString;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
