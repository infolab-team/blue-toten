package br.com.infosolo.blue.core.exception;

import java.util.ArrayList;
import java.util.List;

import br.com.infosolo.blue.model.EntityModel;

public class ValidateException extends Exception {

	private static final long serialVersionUID = 1L;

	private List<String> msgErrors = new ArrayList<String>();

    private EntityModel entityModel;

    public ValidateException(String msg) {
        super(msg);
        msgErrors.add(msg);
    }

    public ValidateException(List<String> msg) {
        msgErrors.addAll(msg);
    }

    public List<String> getMsgErrors() {
        return msgErrors;
    }

    public ValidateException(EntityModel entityModel) {
        this.entityModel = entityModel;
    }

    public EntityModel getEntityModel() {
        return entityModel;
    }
    
}
