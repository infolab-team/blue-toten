package br.com.infosolo.blue.interfaces;

import java.io.Serializable;

public interface ModelEntity<T> extends Serializable {

	public void setId(T id);
	public T getId();

}