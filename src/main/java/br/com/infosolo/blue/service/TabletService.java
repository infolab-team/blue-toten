package br.com.infosolo.blue.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.infosolo.blue.model.Tablet;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.util.IsNullUtils;

@Stateless
public class TabletService extends AbstractService<Tablet>{

	@Inject
	private EntityManager em;
	
	@EJB
	private UsuarioService usuarioService;
	
	public List<Tablet> todos() {
		return em.createNamedQuery("Tablet.findAll", Tablet.class).getResultList();
	}
	
	public Tablet porImei(String numero) {
		List<Tablet> tablet = em.createNamedQuery("Tablet.findByImei", Tablet.class).setParameter("imei", numero).getResultList();
    	return IsNullUtils.isNullOrEmpty(tablet) ? null : tablet.get(0);
	}

	public Tablet cria(Tablet tablet) {
		em.persist(tablet);
		return tablet;
	}
	
	public Tablet editar(String numero, String nome, String versao, String lat, String lng, Integer idAgente) {
		Usuario agente = usuarioService.find(idAgente);
		
		Tablet tablet = porImei(numero);
		tablet.setDataAtualizacao(new Date());
		tablet.setNome(nome);
		tablet.setVersao(versao);
		tablet.setAgenteAtualizacao(agente);

		if(lat != null && !lat.isEmpty()){
			tablet.setLatitude(new BigDecimal(lat));
		}
		
		if(lng != null && !lng.isEmpty()){
			tablet.setLongitude(new BigDecimal(lng));
		}
		
		return em.merge(tablet);
	}

}