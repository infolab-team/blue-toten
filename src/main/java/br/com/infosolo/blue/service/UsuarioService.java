package br.com.infosolo.blue.service;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.util.Mensagens;

@Stateless
public class UsuarioService extends AbstractService<Usuario>{

    @Inject
    private EntityManager em;

    @EJB
    private TransacaoService transacaoService;

    public Usuario load(Integer id) {
        return this.em.find(Usuario.class, id);
    }

    public Usuario toten() {
    	try {
			return this.findByEmail("totenpalmas@infosolo.com.br");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
    
    public Usuario findByEmail(String email) {
        try {
        	Usuario usuario = (Usuario) em.createNamedQuery("Usuario.findByEmail").setParameter("email", email).getSingleResult();
            usuario.setSaldo(new BigDecimal(0));

            return usuario;
            
        } catch (NoResultException ne){
            return null;
        }
        
    }

    public Usuario findByCpfouCnpj(String identificador) throws Exception{
        Usuario usuario = null;
        
        try {
            usuario = (Usuario) em.createNamedQuery("Usuario.findByCpfouCnpj").setParameter("identificador", identificador).getSingleResult();
            usuario.setSaldo(new BigDecimal(0));
        } catch (NoResultException ne){
            throw  new NoResultException(Mensagens.USUARIO_NAO_ENCONTRADO);
        }
        
        return usuario;
    }

    public Usuario findByIdentificadorAndPassword(Usuario _usuario) throws Exception{
        Usuario usuario = null;
        
        try {
            usuario = (Usuario) em.createNamedQuery("Usuario.findByIdentificadorAndPassword")
                    .setParameter("identificador", _usuario.getIdentificador())
                    .setParameter("senha", _usuario.getSenha())
                    .getSingleResult();

            BigDecimal valor = transacaoService.findValorByUsuario(usuario);
            usuario.setSaldo((valor == null ? new BigDecimal(0) : valor));
            
        } catch (NoResultException ne) {
            throw  new NoResultException(Mensagens.USUARIO_NAO_ENCONTRADO);
        }
        
        return usuario;
    }

    public Usuario findByEmailAndPassword(Usuario _usuario) throws Exception{
        Usuario usuario = null;
        
        try {
        	usuario = (Usuario) em.createNamedQuery("Usuario.findByEmailAndPassword")
                    .setParameter("email", _usuario.getEmail())
                    .setParameter("senha", _usuario.getSenha())
                    .getSingleResult();

            BigDecimal valor = transacaoService.findValorByUsuario(usuario);
            usuario.setSaldo((valor == null ? new BigDecimal(0) : valor));
        } catch (NoResultException ne) {
        	System.out.println("Nenhum usuario encontrado para o email " + _usuario.getEmail());
            throw  new NoResultException(Mensagens.USUARIO_NAO_ENCONTRADO);
        }
        
        return usuario;
    }
    
}
