package br.com.infosolo.blue.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.infosolo.blue.model.Preco;
import br.com.infosolo.blue.model.TipoVeiculo;

@Stateless
public class PrecoService {

	@Inject
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Preco> findAll() {
		return em.createNamedQuery("Preco.findAll").getResultList();
	}

	@SuppressWarnings("unchecked")
	public Preco getPrecoByTempo(TipoVeiculo tipoVeiculo, Integer tempoMinutos) {
		List<Preco> precosMaiores = em.createQuery("Select p from Preco p where p.tipoVeiculo = :tipoVeiculo and p.quantidade >= :tempoMinutos order by p.valor asc")
				.setParameter("tipoVeiculo", tipoVeiculo).setParameter("tempoMinutos", tempoMinutos).getResultList();

		if (precosMaiores != null && !precosMaiores.isEmpty()) {
			return precosMaiores.get(0);
		}

		return null;
	}

}