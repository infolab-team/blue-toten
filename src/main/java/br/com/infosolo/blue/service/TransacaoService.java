package br.com.infosolo.blue.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.infosolo.blue.core.exception.ValidateException;
import br.com.infosolo.blue.model.Preco;
import br.com.infosolo.blue.model.TipoVeiculo;
import br.com.infosolo.blue.model.Transacao;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.enums.CategoriaTransacaoEnum;
import br.com.infosolo.blue.model.enums.OrigemTransacao;
import br.com.infosolo.blue.util.IsNullUtils;
import br.com.infosolo.blue.util.Mensagens;
import br.com.infosolo.infra.utils.CPFUtils;
import br.com.infosolo.infra.utils.DateUtils;
import br.com.infosolo.infra.utils.MoedaUtils;
import br.com.infosolo.infra.utils.PropertiesUtils;

@Stateless
public class TransacaoService {

	@Inject
	private EntityManager em;

	@Inject
	private PrecoService precoService;
	
	@EJB
	private MailService mailService;
	
	@Inject
    private Logger logger;
	
	public BigDecimal saldo(Usuario usuario) {
		BigDecimal saldo = (BigDecimal) em.createQuery("select sum(t.valor) from Transacao t where t.usuario = :usuario").setParameter("usuario", usuario).getSingleResult();
		return IsNullUtils.isNull(saldo) ? BigDecimal.ZERO : saldo;
	}
	
	public boolean temSaldoParaCheckin(Usuario usuario, Integer tempoMinutos, TipoVeiculo tipoVeiculo) throws Exception {
		BigDecimal saldoDinheiro = BigDecimal.ZERO;
		saldoDinheiro = this.findValorByUsuario(usuario);
		
		Preco preco = precoService.getPrecoByTempo(tipoVeiculo, tempoMinutos);

		if (preco == null) {
			throw new ValidateException("Não foi possível obter informações sobre o preço do checkin. Tente novamente.");
		}

		BigDecimal saldoPosRenovacao = BigDecimal.ZERO;
		saldoPosRenovacao = saldoDinheiro.subtract(preco.getValor());

		return saldoPosRenovacao.compareTo(BigDecimal.ZERO) >= 0;
	}
	
	public BigDecimal findValorByUsuario(Usuario usuario) throws Exception {
		BigDecimal valor = BigDecimal.ZERO;

		try {
			valor = (BigDecimal) em.createNamedQuery("Transacao.findValorByUsuario").setParameter("idUsuario", usuario.getId()).getSingleResult();
		} catch (NoResultException ne) {
			throw new NoResultException("Não foi encontrado nenhum valor.");
		} 
		
		return valor;
	}
	
	public Transacao load(Integer id){
		return em.find(Transacao.class, id);
	}
	
	
	public void enviaRecibo(Transacao transacao, String email) {
    	if (!IsNullUtils.isNullOrEmpty(email)) {
        	Map<String, String> parametros = new HashMap<String, String>();

        	parametros.put("url_app", PropertiesUtils.getInstance("app").getProperty("app.url"));
        	parametros.put("origem", "Toten");
    		parametros.put("cpf", CPFUtils.format(transacao.getUsuario().getIdentificador()));
    		parametros.put("emissao", DateUtils.dmaAShms.format(transacao.getDataHora()));
       		parametros.put("valor", MoedaUtils.formatar(transacao.getValor()));

    		try {
				mailService.sendEmailForTemplate(email, "Recibo - Compra de Créditos Blue", "recibo_compra_credito.html", parametros);
			} catch (NamingException | MessagingException | IOException e) {
				logger.log(Level.SEVERE, Mensagens.ERRO + e.getMessage());
			}
		}
    }

	public Transacao findBonusRecemConcedido(Usuario usuario) {
		try{
			String sql = "SELECT t FROM Transacao t "
					+ " WHERE t.categoria = :categoriaParam and t.origem = :origemParam"
					+ " and t.praca.id = :pracaIdParam and t.usuario = :usuarioParam "
					+ " and t.dataHora >= :dataParam";
			
			Query query = em.createQuery(sql);
			query.setParameter("categoriaParam", CategoriaTransacaoEnum.BT);
			query.setParameter("origemParam", OrigemTransacao.TT);
			query.setParameter("pracaIdParam", 1); //id de palmas
			query.setParameter("usuarioParam", usuario);
			query.setParameter("dataParam", DateUtils.addMinutsTodate(new Date(), -5));
			query.setMaxResults(1);
			
			return (Transacao) query.getSingleResult();
			
		} catch(NoResultException nre){
			return null;
		}
	}

}