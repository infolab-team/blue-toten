package br.com.infosolo.blue.service.facade;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.infosolo.blue.core.exception.ValidateException;
import br.com.infosolo.blue.model.Checkin;
import br.com.infosolo.blue.model.TipoVeiculo;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.VendaStone;
import br.com.infosolo.blue.model.Zona;
import br.com.infosolo.blue.model.enums.TipoCheckin;
import br.com.infosolo.blue.model.enums.TipoPagamento;
import br.com.infosolo.blue.service.CheckinService;
import br.com.infosolo.blue.service.TipoVeiculoService;
import br.com.infosolo.blue.service.UsuarioService;
import br.com.infosolo.blue.service.VagaService;
import br.com.infosolo.blue.service.ZonaService;

/**
 * Facade de serviços para checkins.
 *
 * @uthor Leonardo da Matta
 */
@Stateless
public class CheckinFacade {

	@EJB
	private CheckinService checkinService;

	@EJB
	private ZonaService zonaService;

	@EJB
	private VagaService vagaService;
	
	@EJB
	private TipoVeiculoService tipoVeiculoService;

	@EJB
	private UsuarioService usuarioService;

	private static final long CARENCIA = 3600000L; // 1 hora

	public Checkin efetuarCheckin(Integer idZona, Integer idTipoVeiculo,
			Integer idUsuario, String placa, Integer permanencia, String tipoCheckin, String tipoPagamento,
			String arn, String ca, String bandeira, String imei) throws Exception {

		Zona zona = zonaService.find(idZona);
		TipoVeiculo tipoVeiculo = tipoVeiculoService.find(idTipoVeiculo);
		Usuario usuario = null;
		
		if(idUsuario != null){
			usuario = usuarioService.find(idUsuario);
		}

		Checkin checkin = checkinService.buscarCheckinEmAberto(placa);

		boolean precisaValidarCarencia = (checkin != null)
				&& (checkin.getZona() != null) 
				&& (checkin.getZona().getNumero().intValue() == idZona.intValue())
				&& (checkin.getDataHoraCheckout() != null);

		if (precisaValidarCarencia) {
			validarCarencia(checkin);
		}

		return validarEEfetivarCheckin(zona, usuario, placa, permanencia, tipoCheckin, tipoPagamento, arn, ca, bandeira, checkin, tipoVeiculo, imei);
	}

	private void validarCarencia(Checkin checkin) throws ValidateException {
		long carenciaCumprida = ((new Date().getTime()) - (checkin.getDataHoraCheckout().getTime()));

		if (carenciaCumprida < CARENCIA) {
			String mensagemDeErro = "Operação não realizada porque ainda faltam " + ((CARENCIA - carenciaCumprida) / 60000) + " minutos de carência a serem cumpridos";
			throw new ValidateException(mensagemDeErro);
		}
	}

	/**
	 * Valida e efetiva um checkin, seja ele avulso ou cadastrado.
	 *
	 * @param idUsuario ID do usuário.
	 * @param placa Placa do veículo que está fazendo o checkin.
	 * @param permanencia Tempo reservado em minutos.
	 * @param tipoCheckin Tipo de checkin.
	 * @param tipoPagamento Tipo de pagamento.
	 * @param arn ARN do cartão de crédito.
	 * @param ca CA do cartão de crédito.
	 * @param bandeira Bandeira do cartão de crédito.
	 * @param vaga Vaga na qual o checkin foi realizado.
	 * @param checkin Checkin.
	 * @param tipoVeiculo Tipo de veiculo que está fazendo o checkin.
	 * @return Checkin atualizado.
	 * @throws Exception Caso algum erro lógico ocorra.
	 */
	private Checkin validarEEfetivarCheckin(Zona zona, Usuario usuario, String placa, Integer permanencia, String tipoCheckin,
			String tipoPagamento, String arn, String ca, String bandeira, Checkin checkin,
			TipoVeiculo tipoVeiculo, String imei) throws Exception {

		//checkin de usuario
		if (TipoCheckin.valueOf(tipoCheckin) == TipoCheckin.U) {
			checkinService.validar(zona, usuario, permanencia, tipoVeiculo);
			checkin = checkinService.efetivar(zona, usuario, permanencia, tipoVeiculo, placa, imei);

		//checkin avulso
		} else if (TipoCheckin.valueOf(tipoCheckin) == TipoCheckin.A) {
			checkinService.validarAvulso(zona, permanencia);
			VendaStone stone = new VendaStone(arn, ca, bandeira);
			checkin = checkinService.efetivarAvulso(zona, permanencia, tipoVeiculo, placa, TipoPagamento.valueOf(tipoPagamento), stone, imei);
		}

		return checkin;
	}
	
}
