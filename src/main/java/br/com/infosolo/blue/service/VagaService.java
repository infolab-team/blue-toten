package br.com.infosolo.blue.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.infosolo.blue.model.Vaga;
import br.com.infosolo.blue.model.Zona;
import br.com.infosolo.blue.model.enums.SubTipoVaga;
import br.com.infosolo.blue.model.enums.TipoVaga;

@Stateless
public class VagaService extends AbstractService<Vaga>{

	@Inject
	private EntityManager em;

	@EJB
	private CheckinService checkinService;

	@EJB
	private ZonaService zonaService;

	public Vaga find(Integer zona, Integer vaga, TipoVaga tipo, SubTipoVaga subTipo) {
		Vaga vagaModel = null;

		if (subTipo == null) {
			vagaModel = (Vaga) em.createNamedQuery("Vaga.findByTipo")
					.setParameter("zona", zona).setParameter("numero", vaga).setParameter("tipoVaga", tipo)
					.getSingleResult();
		} else {
			vagaModel = (Vaga) em.createNamedQuery("Vaga.findByTipoSubTipo")
					.setParameter("zona", zona).setParameter("numero", vaga).setParameter("tipoVaga", tipo)
					.setParameter("subTipoVaga", subTipo).getSingleResult();
		}

		return vagaModel;
	}

	public List<Vaga> findVagasLivresByZona(Zona zona) {
		List<Vaga> vagaModelList = new ArrayList<Vaga>();

		zona = zonaService.find(zona.getId());
		
		List<Vaga> vagasOcupadas = checkinService.buscarVagasOcupadas(zona);
		List<Vaga> vagasZona = findByProperty("zona", zona);

		for (Vaga vaga : vagasZona) {
			if (!vagasOcupadas.contains(vaga)) {
				vagaModelList.add(vaga);
			}
		}
		
		Collections.sort(vagaModelList);

		return vagaModelList;
	}

}