package br.com.infosolo.blue.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.infosolo.blue.model.Zona;
import br.com.infosolo.blue.model.enums.TipoZona;

@Stateless
public class ZonaService extends AbstractService<Zona>{

	@Inject
	private EntityManager em;

	public Zona load(Integer id) {
		return em.find(Zona.class, id);
	}

	public List<Zona> getAll(){
		String sql = "select new br.com.infosolo.blue.model.Zona(z.id, z.numero, z.nome, z.tipo) FROM Zona z where z.praca.id = 1";
		
		TypedQuery<Zona> query = em.createQuery(sql, Zona.class);
		return (List<Zona>) query.getResultList();
	}
	
	public Zona findByNumero(Integer numero) {
		return (Zona) em.createNamedQuery("Zona.findByNumero").setParameter("numero", numero).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<Zona> findAll() {
		return em.createNamedQuery("Zona.findAll").getResultList();
	}

	public List<Zona> getProximasPorCoordenada(String lat, String lng) {
		List<Zona> zonas = new ArrayList<Zona>();
		
		String sql = "SELECT id, nome, tipo, numero, latitude, longitude, limite_maximo, quantidade_vagas, "
				+ " ( 6371 * acos( cos( radians("+lat.toString()+") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians("+lng.toString()+") ) "
				+ " + sin( radians("+lat.toString()+") ) * sin( radians( latitude ) ) ) ) AS distance "
				+ " FROM zona_blue "
				+ " where latitude is not null ORDER BY distance";
		
		logger.info(sql);
		
		Query query = em.createNativeQuery(sql);
		
		List<Object[]> retornoQuery = (List<Object[]>) query.getResultList();
		
		for (Object[] item : retornoQuery) {
			Zona zona = new Zona();
			zona.setId((Integer) item[0]);
			zona.setNome((String) item[1]);
			zona.setTipo(TipoZona.valueOf(((Character) item[2]).toString()));
			zona.setNumero((Integer) item[3]);
			zona.setLat((BigDecimal) item[4]);
			zona.setLng((BigDecimal) item[5]);
			zona.setLimiteMaximo((Integer) item[6]);
			zona.setDistancia((Double) item[8]);
			
			zonas.add(zona);
		}
		
		return zonas;
	}

}