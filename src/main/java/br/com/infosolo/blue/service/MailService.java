package br.com.infosolo.blue.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.ejb.Stateful;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import br.com.infosolo.blue.model.Anexo;
import br.com.infosolo.infra.utils.IsNullUtils;
import br.com.infosolo.infra.utils.PropertiesUtils;

@Stateful
public class MailService implements Serializable {

	private static final long serialVersionUID = 1L;

	public void sendEmail(String destinaray, String subject, String str) throws NamingException, AddressException,
			MessagingException {

		Context initCtx = new InitialContext();
		Session session = (Session) initCtx.lookup(PropertiesUtils.getInstance("app").getProperty("fila_email_jboss"));
		
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(PropertiesUtils.getInstance("app").getProperty("app.mail.from")));
		
		InternetAddress to[] = new InternetAddress[1];
		to[0] = new InternetAddress(destinaray);
		message.setRecipients(Message.RecipientType.TO, to);

		message.setSubject(subject);	
		message.setContent(str, "text/html; charset=\"UTF-8\"");
		Transport.send(message);
	}

	public void sendEmailForTemplate(String destinaray, String subject, String template, Map<String, String> params) throws NamingException, AddressException, MessagingException, IOException {
		try{
			Context initCtx = new InitialContext();
			Session session = (Session) initCtx.lookup(PropertiesUtils.getInstance("app").getProperty("fila_email_jboss"));
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(PropertiesUtils.getInstance("app").getProperty("app.mail.from")));
			
			InternetAddress to[] = new InternetAddress[1];
			to[0] = new InternetAddress(destinaray);
			
			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(subject);
			
			InputStream in = this.getClass().getResourceAsStream("/"+template);
			String content = IOUtils.toString(in);
			
			if (params != null && content != null) {
				for (String key : params.keySet()) {
					content = StringUtils.replace(content, "${"+key+"}", params.get(key));
				}
			}
			
			message.setContent(content, "text/html; charset=\"UTF-8\"");
			Transport.send(message);
			
		} catch(Exception e){
			System.out.println("Não foi possivel enviar email");
			e.printStackTrace();
		}
	}

	public void emailReplicandoComAnexo(String destinationArray, String replyTo, String subject, String html, List<Anexo> anexos) {
		try {
			Context initCtx = new InitialContext();
			Session session = (Session) initCtx.lookup(PropertiesUtils.getInstance("app").getProperty("fila_email_jboss"));
			
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(PropertiesUtils.getInstance("app").getProperty("app.mail.from")));
			
			InternetAddress to[] = new InternetAddress[1];
			to[0] = new InternetAddress(destinationArray);
			
			message.setRecipients(Message.RecipientType.TO, to);
			message.setReplyTo(new InternetAddress[] { new InternetAddress(replyTo) });
			message.setSubject(subject, "UTF-8");
			message.setText(html, "UTF-8");
			
			if (!IsNullUtils.isNullOrEmpty(anexos)) {
				for (Anexo anexo : anexos) {
					Multipart multipart = new MimeMultipart(); 
					MimeBodyPart mimeBodyPart = new MimeBodyPart();
					
					File file = new File("src/main/resources/" + anexo.getNome());
					FileUtils.copyInputStreamToFile(anexo.getArquivo(), file);
					DataSource ds = new FileDataSource(file);
					mimeBodyPart.setDisposition(Part.ATTACHMENT);
					mimeBodyPart.setDataHandler(new DataHandler(ds));
					mimeBodyPart.setFileName(anexo.getNome());

					multipart.addBodyPart(mimeBodyPart);
					
					message.setContent(multipart);
					message.saveChanges();
				}
			}
			
			Transport.send(message); 
		} catch (Exception e) {
			System.out.println("deu ruim em FaleConoscoWithReply. A exception. " + e.getMessage());
		}
	}
	
	public void sendEmail(List<String> destinaray, String subject, String html) throws NamingException, AddressException, MessagingException {
		Context initCtx = new InitialContext();
		Session session = (Session) initCtx.lookup(PropertiesUtils.getInstance("app").getProperty("fila_email_jboss"));
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(PropertiesUtils.getInstance("app").getProperty("app.mail.from")));
		InternetAddress to[] = new InternetAddress[destinaray.size()];

		for (int i = 0; i < destinaray.size(); i++) {
			to[i] = new InternetAddress(destinaray.get(i));
		}

		message.setRecipients(Message.RecipientType.TO, to);
		message.setSubject(subject);
		message.setContent(html, "text/html; charset=\"UTF-8\"");
		Transport.send(message);
	}
	
}