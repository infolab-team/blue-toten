package br.com.infosolo.blue.service;

import java.util.List;

import javax.ejb.Stateless;

import br.com.infosolo.blue.model.TipoVeiculo;

@Stateless
public class TipoVeiculoService extends AbstractService<TipoVeiculo>{

	@SuppressWarnings("unchecked")
	public List<TipoVeiculo> findAllByPracaId(int pracaId) {
		return (List<TipoVeiculo>) em.createNamedQuery("TipoVeiculo.findAll")
								.setParameter("pracaId", pracaId).getResultList();
	}

}
