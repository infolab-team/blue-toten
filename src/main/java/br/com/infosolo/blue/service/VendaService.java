/**
 * 
 */
package br.com.infosolo.blue.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.infosolo.blue.model.Venda;

/**
 * @author Tiago Meneses (tcmeneses@gmail.com)
 * @since Mar 16, 2017
 *
 */
@Stateless
public class VendaService extends AbstractService<Venda> {
	
	@Inject
	private EntityManager em;
	
	public Venda buscarPorCheckin(Integer idCheckin){
		StringBuilder strQuery = new StringBuilder("select v from Venda v where v.checkin.id = :idCheckin");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("idCheckin", idCheckin);
		List<Venda> vendas = query.getResultList();
		return vendas.isEmpty() ? null : vendas.get(0);
	}

}
