package br.com.infosolo.blue.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import br.com.infosolo.blue.core.exception.ValidateException;
import br.com.infosolo.blue.model.Checkin;
import br.com.infosolo.blue.model.EntityModel;
import br.com.infosolo.blue.model.Praca;
import br.com.infosolo.blue.model.Preco;
import br.com.infosolo.blue.model.ReciboCheckin;
import br.com.infosolo.blue.model.Tablet;
import br.com.infosolo.blue.model.TipoVeiculo;
import br.com.infosolo.blue.model.Transacao;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.Vaga;
import br.com.infosolo.blue.model.Venda;
import br.com.infosolo.blue.model.VendaStone;
import br.com.infosolo.blue.model.Zona;
import br.com.infosolo.blue.model.enums.CategoriaTransacaoEnum;
import br.com.infosolo.blue.model.enums.OrigemTransacao;
import br.com.infosolo.blue.model.enums.OrigemVenda;
import br.com.infosolo.blue.model.enums.TipoPagamento;
import br.com.infosolo.blue.model.enums.TipoTransacao;
import br.com.infosolo.blue.util.Mensagens;
import br.com.infosolo.infra.utils.DateUtils;
import br.com.infosolo.infra.utils.IsNullUtils;
import br.com.infosolo.infra.utils.MoedaUtils;
import br.com.infosolo.infra.utils.PropertiesUtils;

@Stateless
public class CheckinService {

	@Inject
	private EntityManager em;

	@EJB
	private PrecoService precoService;

	@EJB
	private UsuarioService usuarioService;
	
	@EJB
	private TransacaoService transacaoService;
	
	@EJB
	private TabletService tabletService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private VendaService vendaService;
	
	@Inject
    private Logger logger;
	

	public Checkin load(Integer id) {
		return this.em.find(Checkin.class, id);
	}

	public Checkin buscarCheckinEmAberto(String placa) throws Exception {
		try {
			String jpql = "SELECT c FROM Checkin c WHERE c.placa = :placa ORDER BY id DESC";
			return (Checkin) em.createQuery(jpql).setParameter("placa", placa).setMaxResults(1).getSingleResult();
			
		} catch (NoResultException ne) {
			return null;
		}
	}

	public void validarAvulso(Zona zona, Integer tempoMinutos) throws ValidateException {
		if (zona.getLimiteMaximo().compareTo(tempoMinutos) < 0) {
			throw new ValidateException("O tempo solicitado excede o limite da vaga em " + (tempoMinutos - zona.getLimiteMaximo()) + " minutos");
		}
	}

	public void validar(Zona zona, Usuario usuario, Integer tempoMinutos, TipoVeiculo tipoVeiculoModel) throws ValidateException {
		EntityModel entityModel = new EntityModel();

		try {
			boolean temSaldo = transacaoService.temSaldoParaCheckin(usuario, tempoMinutos, tipoVeiculoModel);

			if (!temSaldo) {
				entityModel.setSaldo("Usu\u00E1rio n\u00E3o possui saldo suficiente para realizar o checkin");
			}
		} catch (Exception e) {
			entityModel.setException("Erro ao buscar o saldo do usuário. Tente novamente.");
		}

		if (zona.getLimiteMaximo().compareTo(tempoMinutos) < 0) {
			entityModel.setTempo("O tempo solicitado excede o limite da vaga em " + (tempoMinutos - zona.getLimiteMaximo()) + " minutos");
		}

		if (entityModel.isErro()) {
			throw new ValidateException(entityModel);
		}
	}

	public Checkin efetivar(Zona zona, Usuario usuario, Integer tempoMinutos, TipoVeiculo tipoVeiculo, String placa, String imei) throws Exception {
		Praca pracaPalmas = em.find(Praca.class, 1);

		Usuario totem = usuarioService.toten();
		Tablet tablet = tabletService.findOneByProperty("imei", imei);
		
		Checkin checkin = new Checkin();
		checkin.setDataHoraCheckin(new Date());
		checkin.setUsuario(usuario);
		checkin.setUsuarioLogado(totem);
		checkin.setZona(zona);
		checkin.setTempoReservadoMinutos(tempoMinutos);
		checkin.setTablet(tablet);
		
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(checkin.getDataHoraCheckin()); 
		cal.add(Calendar.MINUTE, tempoMinutos); 
		
		Date futureDate = cal.getTime(); 
		
		checkin.setDataHoraCheckout(futureDate);
		checkin.setTipoVeiculo(tipoVeiculo);
		checkin.setPlaca(placa);
		checkin.setOrigem(OrigemVenda.TT);
		checkin.setTablet(tabletService.porImei(imei));
		checkin.setPraca(pracaPalmas);
		em.persist(checkin);

		Preco preco = precoService.getPrecoByTempo(tipoVeiculo, tempoMinutos);
		Transacao transacao = new Transacao();
		transacao.setCheckin(checkin);
		transacao.setDataHora(new Date());
		transacao.setTipoTransacao(TipoTransacao.D);
		transacao.setValor(preco.getValor().multiply(BigDecimal.valueOf(-1)));
		transacao.setUsuario(usuario);
		transacao.setPago(true);
		transacao.setPraca(pracaPalmas);
		transacao.setOrigem(OrigemTransacao.TT);
		transacao.setCategoria(CategoriaTransacaoEnum.CK);
		em.persist(transacao);

		ReciboCheckin reciboCheckin = new ReciboCheckin();
		reciboCheckin.setCheckin(checkin);
		reciboCheckin.setData(new Date());
		reciboCheckin.setPlaca(placa);
		reciboCheckin.setVeiculo("AVULSO");
		reciboCheckin.setValor(preco.getValor());
		reciboCheckin.setZona(checkin.getZona());
		em.persist(reciboCheckin);
		reciboCheckin.setHashId(reciboCheckin.criarHashIdDesteRecibo());
		em.merge(reciboCheckin);

		BigDecimal valor = transacaoService.findValorByUsuario(usuario);
		usuario.setSaldo((valor == null ? new BigDecimal(0) : valor));
		checkin.setUsuario(usuario);
		em.persist(checkin);
		
		return checkin;
	}

	public Checkin efetivarAvulso(Zona zona, Integer tempoMinutos,
			TipoVeiculo tipoVeiculo, String placa, TipoPagamento tipoPagamento, VendaStone stone, String imei) throws Exception {
			
		Praca pracaPalmas = em.find(Praca.class, 1);

		Usuario totem = usuarioService.toten();
		Tablet tablet = tabletService.findOneByProperty("imei", imei);
		
		Date agora = new Date();
		
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(agora); 
		cal.add(Calendar.MINUTE, tempoMinutos); 
		
		Date futureDate = cal.getTime(); 
		
		Checkin checkin = new Checkin();
		checkin.setDataHoraCheckin(agora);
		checkin.setDataHoraCheckout(futureDate);
		checkin.setTempoReservadoMinutos(tempoMinutos);
		checkin.setZona(zona);
		checkin.setUsuarioLogado(totem);
		checkin.setPlaca(placa);
		checkin.setTipoVeiculo(tipoVeiculo);
		checkin.setOrigem(OrigemVenda.TT);
		checkin.setTablet(tabletService.porImei(imei));
		checkin.setPraca(pracaPalmas);
		checkin.setTablet(tablet);
		em.persist(checkin);

		Preco preco = precoService.getPrecoByTempo(tipoVeiculo, tempoMinutos);
		Venda venda = new Venda();
		venda.setDataHora(agora);
		venda.setCheckin(checkin);
		venda.setValor(preco.getValor());
		venda.setAgenteVenda(totem);
		venda.setTipoPagamento(tipoPagamento);
		venda.setOrigem(OrigemVenda.TT);
		venda.setCategoria(CategoriaTransacaoEnum.CK);
		
		em.persist(venda);
		stone.setVenda(venda);
		em.persist(stone);

		ReciboCheckin reciboCheckin = new ReciboCheckin();
		reciboCheckin.setCheckin(checkin);
		reciboCheckin.setData(agora);
		reciboCheckin.setPlaca(placa);
		reciboCheckin.setVeiculo("AVULSO");
		reciboCheckin.setValor(preco.getValor());
		reciboCheckin.setZona(checkin.getZona());
		em.persist(reciboCheckin);
		
		reciboCheckin.setHashId(reciboCheckin.criarHashIdDesteRecibo());
		em.merge(reciboCheckin);
		
		return checkin;
	}

	@SuppressWarnings("unchecked")
	public List<Vaga> buscarVagasOcupadas(Zona zona) {
		String jpql = "select c.vaga from Checkin c where c.dataHoraCheckout is null and c.zona.id = :idZona";
		List<Vaga> vagas = em.createQuery(jpql).setParameter("idZona", zona.getId()).getResultList();

		if (vagas == null) {
			vagas = new ArrayList<Vaga>();
		} else {
			Collections.sort(vagas);
		}

		return vagas;
	}

	public void efetivarCheckoutViaJob(Checkin checkin, Date dataHoraCheckout) throws Exception {
		checkin.setDataHoraCheckout(dataHoraCheckout);
		em.merge(checkin);
		em.flush();
	}
	
    public void enviaRecibo(Checkin checkin, String email) {
    	if (!IsNullUtils.isNullOrEmpty(email)) {
        	Map<String, String> parametros = new HashMap<String, String>();

        	parametros.put("url_app", PropertiesUtils.getInstance("app").getProperty("app.url"));
        	parametros.put("origem", "Totem");
    		parametros.put("placa", checkin.getPlaca());
    		parametros.put("emissao", DateUtils.dmaAShms.format(checkin.getDataHoraCheckin()));
    		
    		String valor = "Não pago";
    		if(checkin.getTransacao() != null && checkin.getUsuario() != null){
    			valor = MoedaUtils.formatar(checkin.getTransacao().getValor());
    			valor = valor.replace("-R$", "R$");
    		}else{
    			Venda venda = vendaService.buscarPorCheckin(checkin.getId());
    			if(venda != null){
    				valor = MoedaUtils.formatar(venda.getValor());
        			valor = valor.replace("-R$", "R$");
    			}
    			
    		}
    		
    		parametros.put("valor", valor);
    		parametros.put("expira", DateUtils.hm.format(checkin.getDataHoraCheckout()));
    		parametros.put("zona", checkin.getZona().getTipo().getDescricao());
    		parametros.put("tempo", checkin.getTempoReservadoMinutos() + " Minutos");
    		parametros.put("valido", DateUtils.dma.format(checkin.getDataHoraCheckout()));

    		try {
				mailService.sendEmailForTemplate(email, "Recibo - Checkin", "recibo_checkin.html", parametros);
			} catch (NamingException | MessagingException | IOException e) {
				logger.log(Level.SEVERE, Mensagens.ERRO + e.getMessage());
			}
		}
    }

}
