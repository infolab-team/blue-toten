package br.com.infosolo.blue.service;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class AbstractService<T> {

	@Inject
    protected Logger logger;
	
	@PersistenceContext
	protected EntityManager em;
	
	@SuppressWarnings("unchecked")
	public T find(Integer id){
		return (T) em.find(getTypeClass(), id);
	}
	
	@SuppressWarnings("unchecked")
	public T find(String idStr){
		return (T) em.find(getTypeClass(), Integer.parseInt(idStr));
	}
	
	public void persist(T entity){
		em.persist(entity);
	}
	
	public T merge(T entity){
		return em.merge(entity);
	}
	
	public List<T> findByProperty(String propertyName, Object value) {
		Class<T> clazz = (Class<T>) getTypeClass();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		Root<T> root = cq.from(clazz);
		cq.where(cb.equal(root.get(propertyName), value));
		
		return em.createQuery(cq).getResultList();
	}
	
	public T findOneByProperty(String propertyName, Object value) {
		List<T> list = findByProperty(propertyName, value);
		
		if(list == null || list.isEmpty()){
			return null;
		}
		
		return list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return em.createQuery(("FROM " + getTypeClass().getName()))
				.getResultList();
	}
	
	private Class<?> getTypeClass() {
		Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		return clazz;
	}
	
}

