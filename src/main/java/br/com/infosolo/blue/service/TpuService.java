package br.com.infosolo.blue.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;

import br.com.infosolo.blue.model.Praca;
import br.com.infosolo.blue.model.Tpu;
import br.com.infosolo.blue.model.Transacao;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.Veiculo;
import br.com.infosolo.blue.model.dto.TpuDTO;
import br.com.infosolo.blue.model.enums.CategoriaTransacaoEnum;
import br.com.infosolo.blue.model.enums.OrigemTransacao;
import br.com.infosolo.blue.model.enums.StatusTPU;
import br.com.infosolo.blue.model.enums.TipoTransacao;
import br.com.infosolo.infra.utils.DateUtils;
import br.com.infosolo.infra.utils.IsNullUtils;
import br.com.infosolo.infra.utils.PropertiesUtils;

@Stateless
public class TpuService {

	@Inject
	private EntityManager em;
	
	@EJB
	private UsuarioService usuarioService;
	
	@EJB
	private TransacaoService transacaoService;
	
	@EJB
	private VeiculoService veiculoService;
	
	@EJB
	private MailService mailService;
	
	public Tpu busca(Integer id) {
		return em.find(Tpu.class, id);
	}

	public List<TpuDTO> porPlaca(String placa) {
		if(placa == null){
			placa = "";
		}
		
		String jpql = "select new br.com.infosolo.blue.model.dto.TpuDTO(tpu.id, tpu.placa, tpu.dataHora, tpu.valor)"
				+ " from Tpu tpu where tpu.placa = :placa and tpu.status = :status order by tpu.id desc";

		return em.createQuery(jpql, TpuDTO.class).setParameter("placa", placa.toUpperCase()).setParameter("status", StatusTPU.R).getResultList();
	}
	
	public boolean usuarioTemSaldoParaPagarTpu(Usuario usuario) {
		return transacaoService.saldo(usuario).compareTo(new BigDecimal(10l)) == 1;
	}
	
    public void enviaRecibo(Tpu tpu, String email) {
    	if (!IsNullUtils.isNullOrEmpty(email)) {
    		
    		Transacao bonus = concedeBonusUsuarioTPU(email, tpu);

    		Map<String, String> parametros = new HashMap<String, String>();

        	parametros.put("url_app", PropertiesUtils.getInstance("app").getProperty("app.url"));
        	parametros.put("numero", tpu.getId() + "");
        	parametros.put("placa", tpu.getPlaca());
    		parametros.put("emissao", DateUtils.dmaAShms.format(tpu.getDataHora()));
    		parametros.put("pagamento", DateUtils.dmaAShms.format(tpu.getDataHoraPagamento()));
    		parametros.put("valorTpu", formatar(new BigDecimal(tpu.getValor())));
    		parametros.put("valorBonus", bonus != null ? formatar(bonus.getValor()) : "R$ 0,00");

    		try {
				mailService.sendEmailForTemplate(email, "Recibo - TPU", "recibo_quitacao_tpu.html", parametros);
				
			} catch (NamingException | MessagingException | IOException e) {
				e.printStackTrace();
			}
		}
    }
    
    private Transacao concedeBonusUsuarioTPU(String email, Tpu tpu){
    	//Recupera usuario por email
    	Usuario usuario = usuarioService.findByEmail(email);
    	
    	//Recupera usuario por placa da tpu
    	if(usuario == null){
    		List<Veiculo> veiculos = veiculoService.findByProperty("placa", tpu.getPlaca());
    		
    		if(veiculos != null && veiculos.size() == 1){
    			usuario = veiculos.get(0).getUsuario();
    		}
    	}

    	if(usuario != null){
    		Transacao bonusRecente = transacaoService.findBonusRecemConcedido(usuario);
    		
    		if(bonusRecente == null){
    			Praca pracaPalmas = em.find(Praca.class, 1);
    			
    			Transacao bonus = new Transacao();
    			bonus.setDataHora(new Date());
    			bonus.setTipoTransacao(TipoTransacao.C);
    			bonus.setOrigem(OrigemTransacao.TT);
    			bonus.setCategoria(CategoriaTransacaoEnum.BT);
    			bonus.setPago(true);
    			bonus.setUsuario(usuario);
    			bonus.setValor(new BigDecimal(tpu.getValor()).multiply(new BigDecimal(0.5)).abs());
    			bonus.setPraca(pracaPalmas);
    			em.persist(bonus);
    			
    			em.flush();
    			
    			return bonus;
    		}
    	}
    	
    	return null;
    }
    
    public static String formatar(BigDecimal valor) {
		DecimalFormat formatar = new DecimalFormat("'R$' #,###,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
		return formatar.format(IsNullUtils.isNull(valor) ? BigDecimal.ZERO : valor);
	}

}