package br.com.infosolo.blue.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.infosolo.blue.model.Praca;
import br.com.infosolo.blue.model.ReciboVenda;
import br.com.infosolo.blue.model.Tablet;
import br.com.infosolo.blue.model.Tpu;
import br.com.infosolo.blue.model.Transacao;
import br.com.infosolo.blue.model.Usuario;
import br.com.infosolo.blue.model.Venda;
import br.com.infosolo.blue.model.VendaStone;
import br.com.infosolo.blue.model.enums.CategoriaTransacaoEnum;
import br.com.infosolo.blue.model.enums.OrigemTransacao;
import br.com.infosolo.blue.model.enums.OrigemVenda;
import br.com.infosolo.blue.model.enums.StatusTPU;
import br.com.infosolo.blue.model.enums.TipoPagamento;
import br.com.infosolo.blue.model.enums.TipoTransacao;
import br.com.infosolo.blue.util.IsNullUtils;

@Stateless
public class VendaCreditoService {

	@Inject
	private Logger logger;
	
	@Inject
	private EntityManager em;
	
	@EJB
	private UsuarioService usuarioService;
	
	public Transacao efetivar(Usuario usuario, Tablet tablet, BigDecimal valor, TipoPagamento tipoPagamento, VendaStone stone) {
		Praca pracaPalmas = em.find(Praca.class, 1);

		Venda venda = new Venda();
		venda.setComprador(usuario);
		venda.setValor(valor);
		venda.setTipoPagamento(tipoPagamento);
		venda.setOrigem(OrigemVenda.TT);
		venda.setCategoria(CategoriaTransacaoEnum.VC);
		venda.setTablet(tablet);
		venda.setDataHora(new Date());

		em.persist(venda);

		logger.log(Level.INFO, "ID Venda: " + venda.getId());
		
		Transacao transacao = new Transacao();
		transacao.setTipoTransacao(TipoTransacao.C);
		transacao.setVenda(venda);
		transacao.setUsuario(usuario);
		transacao.setValor(valor);
		transacao.setDataHora(new Date());
		transacao.setPago(true);
		transacao.setOrigem(OrigemTransacao.TT);
		transacao.setCategoria(CategoriaTransacaoEnum.CC);
		transacao.setPraca(pracaPalmas);

		em.persist(transacao);

		logger.log(Level.INFO, "ID Transacao: " + transacao.getId());
		
		ReciboVenda recibo = new ReciboVenda();
		recibo.setVenda(venda);
		recibo.setDataHoraEmissao(new Date());

		em.persist(recibo);
		em.flush();

		logger.log(Level.INFO, "ID Recibo Venda: " + recibo.getId());
		logger.log(Level.INFO, "Tipo pagamento: " + recibo.getId());
		
		if ((TipoPagamento.C.equals(tipoPagamento) || TipoPagamento.D.equals(tipoPagamento)) && stone != null) {
			logger.log(Level.INFO, "Entrou salvar stone");
			stone.setVenda(venda);
			em.persist(stone);
		}

		em.flush();
		
		return transacao;
	}
	
	public Tpu pagamentoComCreditoBlue(Tpu tpu, Usuario usuario) {
		Date now = new Date();
		
		tpu.setStatus(StatusTPU.Q);
        tpu.setDataHoraPagamento(now);
        tpu.setUsuario(usuario);
        em.merge(tpu);

        Praca pracaPalmas = em.find(Praca.class, 1);
        
        Transacao transacao = new Transacao();
        transacao.setDataHora(now);
        transacao.setTipoTransacao(TipoTransacao.D);
        transacao.setOrigem(OrigemTransacao.TT);
        transacao.setCategoria(CategoriaTransacaoEnum.PT);
        transacao.setPago(true);
        transacao.setUsuario(usuario);
        transacao.setValor(new BigDecimal(tpu.getValor() * (-1)));
        transacao.setPraca(pracaPalmas);
        em.persist(transacao);
        
        Transacao bonus = new Transacao();
        bonus.setDataHora(now);
        bonus.setTipoTransacao(TipoTransacao.C);
        bonus.setOrigem(OrigemTransacao.TT);
        bonus.setCategoria(CategoriaTransacaoEnum.BT);
        bonus.setPago(true);
        bonus.setUsuario(usuario);
        bonus.setValor(transacao.getValor().multiply(new BigDecimal(0.5)).abs());
        bonus.setPraca(pracaPalmas);
        em.persist(bonus);
        
        em.flush();
        
        return tpu;
    }
	
	public void pagamentoComCartaoDeCredito(Tpu tpu, TipoPagamento tipoPagamento, VendaStone stone) {
		Usuario usuarioToten = usuarioService.toten();
		
		Venda venda = new Venda(tpu.getValor(), tipoPagamento);
        venda.setAgenteVenda(usuarioToten);
        venda.setOrigem(OrigemVenda.TT);
        venda.setCategoria(CategoriaTransacaoEnum.PT);
		
		this.em.persist(venda);

        if (venda.temComprador()) {
            Transacao credito = new Transacao();
            credito.setTipoTransacao(TipoTransacao.C);
            credito.setVenda(venda);
            credito.setValor(venda.getValor());
            credito.setDataHora(new Date());
            credito.setUsuario(venda.getComprador());
            this.em.persist(credito);

            Transacao debito = new Transacao();
            debito.setTipoTransacao(TipoTransacao.D);
            debito.setVenda(venda);
            debito.setValor(venda.getValor().multiply(BigDecimal.valueOf(-1)));
            debito.setDataHora(new Date());
            debito.setUsuario(venda.getComprador());
            this.em.persist(debito);
        }

        tpu.setStatus(StatusTPU.Q);
        tpu.setDataHoraPagamento(new Date());
        tpu.setVenda(venda);
        this.em.merge(tpu);
        
        venda.setTpu(tpu);
        this.em.merge(venda);
        
        if (IsNullUtils.isNotNull(stone)) {
            stone.setVenda(venda);
            this.em.persist(stone);
        }
        
        this.em.flush();
    }
	
//	private void efetuaRegularizacaoComAreatec(TPU tpu) {
//		Xml actXml = AreaTecnologia.efetivarRegularizacao(tpu.getPlaca(), tpu.getNumero(), tpu.getDataHoraPagamento());
//    	
//    	if (AreaTecnologia.FALHA.equals(actXml.getRetornoACT().getCodigoACT())) {
//			throw new EJBException("Falha ao tentar regularizar ACT.");
//		}
//    }
	
}
